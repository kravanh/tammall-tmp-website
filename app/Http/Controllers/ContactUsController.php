<?php

namespace App\Http\Controllers;

use App\ContactUs;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    public function index() {
        $contact_us = ContactUs::orderBy('id','DESC')->paginate(10);
        return view('backend.contact_us.create', compact('contact_us'));
    }
}
