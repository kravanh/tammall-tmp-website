<?php

namespace App\Http\Controllers;

use App\SlideShow;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class SlideController extends Controller
{
    public function index() {
        $slide = SlideShow::orderBy('order','ASC')->paginate(10);
        return view('backend.slide.index',compact('slide'));
    }

    public function create(){
        return view('backend.slide.create');
    }
    public function store(Request $request)
    {
        $image_id=0;
        if($request['files']){
            $image_id = $request['files']['slide'];
        }
        $slide = new SlideShow();
        $slide->order = $request->input('order');
        $slide->name = $request->input('name');
        $slide->image_id = $image_id;
        $slide->save();

        //Save Translation

        if(! $request->ajax()){
            return redirect()->route('slide.create')->with('success', _lang('Saved Sucessfully'));
        }else{
            return response()->json(['result'=>'success','action'=>'store','message'=>_lang('Saved Sucessfully'),'data'=>$slide, 'table' => '#brands_table']);
        }

    }
    public function edit($id){
        $slide = SlideShow::findOrFail($id);
        return view('backend.slide.edit',compact('slide'));
    }

    public function update(Request $request){
        $image_id=0;
        if($request['files']){
            $image_id = $request['files']['slide'];
        }
        $slide = SlideShow::findOrFail($request->id);
        $slide->order = $request->input('order');
        $slide->name = $request->input('name');
        $slide->image_id = $image_id;
        $slide->save();

        //Save Translation

        if(! $request->ajax()){
            return redirect()->route('slide.edit',['id'=>$request->id])->with('success', _lang('Saved Sucessfully'));
        }else{
            return response()->json(['result'=>'success','action'=>'store','message'=>_lang('Saved Sucessfully'),'data'=>$slide, 'table' => '#brands_table']);
        }
    }

    public function destroy($id){

        $slide = SlideShow::findOrFail($id);
        $slide->delete();

        //Save Translation

        return redirect()->route('slide.index')->with('success',_lang('Deleted Sucessfully'));
    }
}
