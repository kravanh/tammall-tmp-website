<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SlideShow extends Model
{
    public function file()
    {
        return $this->hasOne('App\Media','id','image_id');
    }
}
