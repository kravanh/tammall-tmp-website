DROP TABLE IF EXISTS database_backups;

CREATE TABLE `database_backups` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS email_templates;

CREATE TABLE `email_templates` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS failed_jobs;

CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS migrations;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO migrations VALUES('1','2014_10_12_000000_create_users_table','1');
INSERT INTO migrations VALUES('2','2014_10_12_100000_create_password_resets_table','1');
INSERT INTO migrations VALUES('3','2019_08_19_000000_create_failed_jobs_table','1');
INSERT INTO migrations VALUES('4','2019_09_01_080940_create_settings_table','2');
INSERT INTO migrations VALUES('5','2018_11_12_152015_create_email_templates_table','3');
INSERT INTO migrations VALUES('6','2020_07_02_145857_create_database_backups_table','4');



DROP TABLE IF EXISTS password_resets;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS settings;

CREATE TABLE `settings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO settings VALUES('1','company_name','Tricky Code','2020-07-02 13:50:52','2020-07-02 21:47:52');
INSERT INTO settings VALUES('2','site_title','Laravel 7 Starter','2020-07-02 13:50:52','2020-07-02 21:47:53');
INSERT INTO settings VALUES('3','phone','','2020-07-02 13:50:52','2020-07-02 21:47:53');
INSERT INTO settings VALUES('4','email','','2020-07-02 13:50:52','2020-07-02 21:47:53');
INSERT INTO settings VALUES('5','timezone','Asia/Dhaka','2020-07-02 13:50:52','2020-07-02 21:47:53');
INSERT INTO settings VALUES('6','language','English','2020-07-02 13:50:52','2020-07-02 21:47:53');
INSERT INTO settings VALUES('7','address','','2020-07-02 13:50:52','2020-07-02 21:47:53');
INSERT INTO settings VALUES('8','membership_system','enabled','2020-07-02 21:50:46','2020-07-02 21:50:46');
INSERT INTO settings VALUES('9','allow_singup','yes','2020-07-02 21:50:46','2020-07-02 21:50:46');
INSERT INTO settings VALUES('10','email_verification','enabled','2020-07-02 21:50:46','2020-07-02 21:50:46');
INSERT INTO settings VALUES('11','trial_period','0','2020-07-02 21:50:46','2020-07-02 21:50:46');
INSERT INTO settings VALUES('12','currency','USD','2020-07-02 21:50:46','2020-07-02 21:50:46');
INSERT INTO settings VALUES('13','currency_position','left','2020-07-02 21:50:46','2020-07-02 21:50:46');
INSERT INTO settings VALUES('14','website_enable','yes','2020-07-02 21:51:00','2020-07-02 21:55:49');
INSERT INTO settings VALUES('15','currency_converter','manual','2020-07-02 21:51:00','2020-07-02 21:55:49');
INSERT INTO settings VALUES('16','fixer_api_key','','2020-07-02 21:51:00','2020-07-02 21:55:49');
INSERT INTO settings VALUES('17','date_format','d/m/Y','2020-07-02 21:51:00','2020-07-02 21:55:49');
INSERT INTO settings VALUES('18','time_format','24','2020-07-02 21:51:00','2020-07-02 21:55:49');
INSERT INTO settings VALUES('19','file_manager_file_type_supported','png,jpg,jpeg','2020-07-02 21:51:00','2020-07-02 21:55:49');
INSERT INTO settings VALUES('20','file_manager_max_upload_size','2','2020-07-02 21:51:00','2020-07-02 21:55:49');



DROP TABLE IF EXISTS users;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `profile_picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO users VALUES('1','admin','admin@demo.com','admin','1','default.png','','$2y$10$vHTtwzZutFuRuUYLx6mFLO2M/hoPwmWORi8B0x1YcfQQlMmXDXsp6','','2020-07-02 06:08:49','2020-07-02 13:15:47');



