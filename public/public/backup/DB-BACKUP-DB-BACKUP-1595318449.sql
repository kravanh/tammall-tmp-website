DROP TABLE IF EXISTS database_backups;

CREATE TABLE `database_backups` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO database_backups VALUES('2','DB-BACKUP-1593705387.sql','1','2020-07-02 21:56:27','2020-07-02 21:56:27');



DROP TABLE IF EXISTS email_templates;

CREATE TABLE `email_templates` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS failed_jobs;

CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS migrations;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO migrations VALUES('1','2014_10_12_000000_create_users_table','1');
INSERT INTO migrations VALUES('2','2014_10_12_100000_create_password_resets_table','1');
INSERT INTO migrations VALUES('3','2019_08_19_000000_create_failed_jobs_table','1');
INSERT INTO migrations VALUES('4','2019_09_01_080940_create_settings_table','2');
INSERT INTO migrations VALUES('5','2018_11_12_152015_create_email_templates_table','3');
INSERT INTO migrations VALUES('6','2020_07_02_145857_create_database_backups_table','4');
INSERT INTO migrations VALUES('7','2020_07_06_142817_create_roles_table','5');
INSERT INTO migrations VALUES('8','2020_07_06_143240_create_permissions_table','5');



DROP TABLE IF EXISTS password_resets;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS permissions;

CREATE TABLE `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NOT NULL,
  `permission` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO permissions VALUES('10','1','users.index','2020-07-20 08:16:42','2020-07-20 08:16:42');
INSERT INTO permissions VALUES('11','1','users.show','2020-07-20 08:16:42','2020-07-20 08:16:42');
INSERT INTO permissions VALUES('12','1','email_templates.index','2020-07-20 08:16:42','2020-07-20 08:16:42');
INSERT INTO permissions VALUES('13','1','email_templates.create','2020-07-20 08:16:42','2020-07-20 08:16:42');
INSERT INTO permissions VALUES('14','1','email_templates.show','2020-07-20 08:16:42','2020-07-20 08:16:42');



DROP TABLE IF EXISTS roles;

CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO roles VALUES('1','Manager','','2020-07-06 21:23:55','2020-07-06 21:23:55');



DROP TABLE IF EXISTS settings;

CREATE TABLE `settings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO settings VALUES('1','company_name','Tricky Code','2020-07-02 13:50:52','2020-07-19 21:07:01');
INSERT INTO settings VALUES('2','site_title','Laravel 7 Starter','2020-07-02 13:50:52','2020-07-19 21:07:01');
INSERT INTO settings VALUES('3','phone','','2020-07-02 13:50:52','2020-07-19 21:07:01');
INSERT INTO settings VALUES('4','email','','2020-07-02 13:50:52','2020-07-19 21:07:01');
INSERT INTO settings VALUES('5','timezone','Asia/Dhaka','2020-07-02 13:50:52','2020-07-19 21:07:01');
INSERT INTO settings VALUES('6','language','English','2020-07-02 13:50:52','2020-07-19 21:07:01');
INSERT INTO settings VALUES('7','address','','2020-07-02 13:50:52','2020-07-19 21:07:01');
INSERT INTO settings VALUES('8','membership_system','enabled','2020-07-02 21:50:46','2020-07-02 21:50:46');
INSERT INTO settings VALUES('9','allow_singup','yes','2020-07-02 21:50:46','2020-07-20 20:13:40');
INSERT INTO settings VALUES('10','email_verification','enabled','2020-07-02 21:50:46','2020-07-20 20:13:40');
INSERT INTO settings VALUES('11','trial_period','0','2020-07-02 21:50:46','2020-07-02 21:50:46');
INSERT INTO settings VALUES('12','currency','USD','2020-07-02 21:50:46','2020-07-20 20:13:39');
INSERT INTO settings VALUES('13','currency_position','left','2020-07-02 21:50:46','2020-07-20 20:13:39');
INSERT INTO settings VALUES('14','website_enable','yes','2020-07-02 21:51:00','2020-07-20 20:13:39');
INSERT INTO settings VALUES('15','currency_converter','manual','2020-07-02 21:51:00','2020-07-20 20:13:39');
INSERT INTO settings VALUES('16','fixer_api_key','','2020-07-02 21:51:00','2020-07-20 20:13:39');
INSERT INTO settings VALUES('17','date_format','d/m/Y','2020-07-02 21:51:00','2020-07-20 20:13:39');
INSERT INTO settings VALUES('18','time_format','24','2020-07-02 21:51:00','2020-07-20 20:13:40');
INSERT INTO settings VALUES('19','file_manager_file_type_supported','png,jpg,jpeg','2020-07-02 21:51:00','2020-07-20 20:13:40');
INSERT INTO settings VALUES('20','file_manager_max_upload_size','2','2020-07-02 21:51:00','2020-07-20 20:13:40');
INSERT INTO settings VALUES('21','paypal_active','Yes','2020-07-06 16:29:48','2020-07-06 16:33:12');
INSERT INTO settings VALUES('22','paypal_email','','2020-07-06 16:29:48','2020-07-06 16:33:12');
INSERT INTO settings VALUES('23','paypal_currency','USD','2020-07-06 16:29:48','2020-07-06 16:33:12');
INSERT INTO settings VALUES('24','stripe_active','Yes','2020-07-06 16:29:48','2020-07-06 16:33:12');
INSERT INTO settings VALUES('25','stripe_secret_key','','2020-07-06 16:29:48','2020-07-06 16:33:12');
INSERT INTO settings VALUES('26','stripe_publishable_key','','2020-07-06 16:29:49','2020-07-06 16:33:12');
INSERT INTO settings VALUES('27','stripe_currency','USD','2020-07-06 16:29:49','2020-07-06 16:33:12');
INSERT INTO settings VALUES('28','google_login','enabled','2020-07-06 16:34:56','2020-07-20 22:00:22');
INSERT INTO settings VALUES('29','GOOGLE_CLIENT_ID','3387463203-c4gaok5d8rt9mlqfkikq48o8kocqhasq.apps.googleusercontent.com','2020-07-06 16:34:57','2020-07-20 22:00:22');
INSERT INTO settings VALUES('30','GOOGLE_CLIENT_SECRET','0qiB_suRnDhEL0Ql8B0Zvzqy','2020-07-06 16:34:58','2020-07-20 22:00:22');
INSERT INTO settings VALUES('31','backedn_direction','rtl','2020-07-06 16:37:52','2020-07-06 16:37:52');
INSERT INTO settings VALUES('32','backend_direction','ltr','2020-07-06 16:39:09','2020-07-20 20:13:39');
INSERT INTO settings VALUES('34','facebook_login','disabled','2020-07-20 21:37:05','2020-07-20 22:00:23');
INSERT INTO settings VALUES('35','FACEBOOK_CLIENT_ID','','2020-07-20 21:37:05','2020-07-20 22:00:23');
INSERT INTO settings VALUES('36','FACEBOOK_CLIENT_SECRET','','2020-07-20 21:37:05','2020-07-20 22:00:23');



DROP TABLE IF EXISTS users;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `profile_picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO users VALUES('1','admin','admin@demo.com','admin','','1','default.png','','$2y$10$vHTtwzZutFuRuUYLx6mFLO2M/hoPwmWORi8B0x1YcfQQlMmXDXsp6','','','','2020-07-02 06:08:49','2020-07-02 13:15:47');
INSERT INTO users VALUES('5','User','user@gmail.com','user','1','1','','2020-07-20 14:15:53','$2y$10$pc0BWdb80/ugNRHyZmpwluhuRFYcaaRVWAbHDHXq/fzHeAU3LAn7O','','','','2020-07-20 14:15:53','2020-07-20 14:15:53');
INSERT INTO users VALUES('6','Test User','testuser@gmail.com','admin','','1','','2020-07-20 15:18:14','$2y$10$l0kaRARov.o9maYvU9463.EjnDuzuS3/2IXkjp61K00MYjO70IceS','','','','2020-07-20 15:18:14','2020-07-20 15:18:14');
INSERT INTO users VALUES('9','Francis kennedy rodrick','kennedyrodrick93@gmail.com','user','','1','1595262364_avatar.jpg','','','google','107061215466680206094','','2020-07-20 16:26:04','2020-07-20 16:26:04');



