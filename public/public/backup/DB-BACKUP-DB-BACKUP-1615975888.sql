DROP TABLE IF EXISTS brands;

CREATE TABLE `brands` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `brands_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO brands VALUES('1','sony','2020-11-02 22:21:51','2020-11-02 22:21:51');
INSERT INTO brands VALUES('2','samsung','2020-11-02 22:22:00','2020-11-02 22:22:00');
INSERT INTO brands VALUES('3','rolex','2020-11-02 22:22:11','2020-11-02 22:22:11');
INSERT INTO brands VALUES('4','puma','2020-11-02 22:22:19','2020-11-02 22:22:19');
INSERT INTO brands VALUES('5','omega','2020-11-02 22:22:29','2020-11-02 22:22:29');
INSERT INTO brands VALUES('6','nike','2020-11-02 22:22:37','2020-11-02 22:22:37');
INSERT INTO brands VALUES('7','microsoft','2020-11-02 22:22:51','2020-11-02 22:22:51');
INSERT INTO brands VALUES('8','lg','2020-11-02 22:23:01','2020-11-02 22:23:01');
INSERT INTO brands VALUES('9','hp','2020-11-02 22:23:08','2020-11-02 22:23:08');
INSERT INTO brands VALUES('10','dell','2020-11-02 22:23:16','2020-11-02 22:23:16');
INSERT INTO brands VALUES('11','adidas','2020-11-02 22:23:29','2020-11-02 22:23:29');
INSERT INTO brands VALUES('12','apple','2020-11-02 22:23:39','2020-11-02 22:23:39');



DROP TABLE IF EXISTS brands_translation;

CREATE TABLE `brands_translation` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `brand_id` bigint(20) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `brands_translation_brand_id_locale_unique` (`brand_id`,`locale`),
  CONSTRAINT `brands_translation_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO brands_translation VALUES('1','1','English','Sony','2020-11-02 22:21:51','2020-11-02 22:21:51');
INSERT INTO brands_translation VALUES('2','2','English','Samsung','2020-11-02 22:22:00','2020-11-02 22:22:00');
INSERT INTO brands_translation VALUES('3','3','English','Rolex','2020-11-02 22:22:11','2020-11-02 22:22:11');
INSERT INTO brands_translation VALUES('4','4','English','Puma','2020-11-02 22:22:19','2020-11-02 22:22:19');
INSERT INTO brands_translation VALUES('5','5','English','Omega','2020-11-02 22:22:29','2020-11-02 22:22:29');
INSERT INTO brands_translation VALUES('6','6','English','Nike','2020-11-02 22:22:37','2020-11-02 22:22:37');
INSERT INTO brands_translation VALUES('7','7','English','Microsoft','2020-11-02 22:22:51','2020-11-02 22:22:51');
INSERT INTO brands_translation VALUES('8','8','English','LG','2020-11-02 22:23:01','2020-11-02 22:23:01');
INSERT INTO brands_translation VALUES('9','9','English','HP','2020-11-02 22:23:08','2020-11-02 22:23:08');
INSERT INTO brands_translation VALUES('10','10','English','Dell','2020-11-02 22:23:16','2020-11-02 22:23:16');
INSERT INTO brands_translation VALUES('11','11','English','Adidas','2020-11-02 22:23:29','2020-11-02 22:23:29');
INSERT INTO brands_translation VALUES('12','12','English','Apple','2020-11-02 22:23:39','2020-11-02 22:23:39');



DROP TABLE IF EXISTS category;

CREATE TABLE `category` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO category VALUES('1','electronic-devices','','2020-11-02 22:24:37','2020-11-02 22:24:37');
INSERT INTO category VALUES('2','desktop-components','','2020-11-02 22:24:51','2020-11-02 22:27:43');
INSERT INTO category VALUES('3','health-&-beauty','','2020-11-02 22:25:14','2020-11-02 22:25:14');
INSERT INTO category VALUES('4','men\'s-fashion','','2020-11-02 22:25:30','2020-11-02 22:25:30');
INSERT INTO category VALUES('5','women\'s-fashion','','2020-11-02 22:25:47','2020-11-02 22:25:47');
INSERT INTO category VALUES('6','sports-&-outdoor','','2020-11-02 22:26:02','2020-11-02 22:26:02');
INSERT INTO category VALUES('7','mobile','1','2020-11-02 22:26:23','2020-11-02 22:26:23');
INSERT INTO category VALUES('8','laptop','1','2020-11-02 22:26:33','2020-11-02 22:26:33');
INSERT INTO category VALUES('9','cameras','1','2020-11-02 22:26:46','2020-11-02 22:26:46');
INSERT INTO category VALUES('10','tablets','1','2020-11-02 22:27:06','2020-11-02 22:27:06');
INSERT INTO category VALUES('11','hair-care','3','2020-11-02 22:28:18','2020-11-02 22:28:18');
INSERT INTO category VALUES('12','skin-care','3','2020-11-02 22:28:27','2020-11-02 22:28:27');
INSERT INTO category VALUES('13','food-supliments','3','2020-11-02 22:28:58','2020-11-02 22:28:58');
INSERT INTO category VALUES('14','t-shirts','4','2020-11-02 22:29:20','2020-11-02 22:29:20');
INSERT INTO category VALUES('15','shirts','4','2020-11-02 22:29:30','2020-11-02 22:29:30');
INSERT INTO category VALUES('16','jeans','4','2020-11-02 22:29:42','2020-11-02 22:29:42');
INSERT INTO category VALUES('17','shoes','4','2020-11-02 22:29:58','2020-11-02 22:29:58');
INSERT INTO category VALUES('18','women\'s-bags','5','2020-11-02 22:31:04','2020-11-02 22:31:04');
INSERT INTO category VALUES('19','women\'s-shoes','5','2020-11-02 22:31:18','2020-11-02 22:31:18');
INSERT INTO category VALUES('20','kurti','5','2020-11-02 22:31:56','2020-11-02 22:31:56');
INSERT INTO category VALUES('21','fitness-accessories','6','2020-11-02 22:32:35','2020-11-02 22:32:35');
INSERT INTO category VALUES('22','team-sports','6','2020-11-02 22:32:48','2020-11-02 22:32:48');
INSERT INTO category VALUES('23','treadmills','6','2020-11-02 22:33:27','2020-11-02 22:33:27');
INSERT INTO category VALUES('24','software','','2020-11-03 19:04:57','2020-11-03 19:04:57');
INSERT INTO category VALUES('25','motherboard','2','2021-03-08 13:57:19','2021-03-08 13:57:19');
INSERT INTO category VALUES('26','adobe-photoshop','24','2021-03-08 13:57:33','2021-03-08 13:57:33');



DROP TABLE IF EXISTS category_translation;

CREATE TABLE `category_translation` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` bigint(20) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category_translation_category_id_locale_unique` (`category_id`,`locale`),
  CONSTRAINT `category_translation_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO category_translation VALUES('1','1','English','Electronic Devices','','2020-11-02 22:24:37','2020-11-02 22:24:37');
INSERT INTO category_translation VALUES('2','2','English','Desktop Components','','2020-11-02 22:24:51','2020-11-02 22:27:43');
INSERT INTO category_translation VALUES('3','3','English','Health & Beauty','','2020-11-02 22:25:14','2020-11-02 22:25:14');
INSERT INTO category_translation VALUES('4','4','English','Men\'s Fashion','','2020-11-02 22:25:30','2020-11-02 22:25:30');
INSERT INTO category_translation VALUES('5','5','English','Women\'s Fashion','','2020-11-02 22:25:47','2020-11-02 22:25:47');
INSERT INTO category_translation VALUES('6','6','English','Sports & Outdoor','','2020-11-02 22:26:02','2020-11-02 22:26:02');
INSERT INTO category_translation VALUES('7','7','English','Mobile','','2020-11-02 22:26:23','2020-11-02 22:26:23');
INSERT INTO category_translation VALUES('8','8','English','Laptop','','2020-11-02 22:26:33','2020-11-02 22:26:33');
INSERT INTO category_translation VALUES('9','9','English','Cameras','','2020-11-02 22:26:46','2020-11-02 22:26:46');
INSERT INTO category_translation VALUES('10','10','English','Tablets','','2020-11-02 22:27:06','2020-11-02 22:27:06');
INSERT INTO category_translation VALUES('11','11','English','Hair Care','','2020-11-02 22:28:18','2020-11-02 22:28:18');
INSERT INTO category_translation VALUES('12','12','English','Skin care','','2020-11-02 22:28:27','2020-11-02 22:28:27');
INSERT INTO category_translation VALUES('13','13','English','Food Supliments','','2020-11-02 22:28:58','2020-11-02 22:28:58');
INSERT INTO category_translation VALUES('14','14','English','T-Shirts','','2020-11-02 22:29:20','2020-11-02 22:29:20');
INSERT INTO category_translation VALUES('15','15','English','Shirts','','2020-11-02 22:29:30','2020-11-02 22:29:30');
INSERT INTO category_translation VALUES('16','16','English','Jeans','','2020-11-02 22:29:42','2020-11-02 22:29:42');
INSERT INTO category_translation VALUES('17','17','English','Shoes','','2020-11-02 22:29:58','2020-11-02 22:29:58');
INSERT INTO category_translation VALUES('18','18','English','Women\'s Bags','','2020-11-02 22:31:04','2020-11-02 22:31:04');
INSERT INTO category_translation VALUES('19','19','English','Women\'s Shoes','','2020-11-02 22:31:18','2020-11-02 22:31:18');
INSERT INTO category_translation VALUES('20','20','English','Kurti','','2020-11-02 22:31:56','2020-11-02 22:31:56');
INSERT INTO category_translation VALUES('21','21','English','Fitness Accessories','','2020-11-02 22:32:35','2020-11-02 22:32:35');
INSERT INTO category_translation VALUES('22','22','English','Team Sports','','2020-11-02 22:32:48','2020-11-02 22:32:48');
INSERT INTO category_translation VALUES('23','23','English','Treadmills','','2020-11-02 22:33:27','2020-11-02 22:33:27');
INSERT INTO category_translation VALUES('24','24','English','Software','','2020-11-03 19:04:57','2020-11-03 19:04:57');
INSERT INTO category_translation VALUES('25','25','English','Motherboard','','2021-03-08 13:57:19','2021-03-08 13:57:19');
INSERT INTO category_translation VALUES('26','26','English','Adobe Photoshop','','2021-03-08 13:57:33','2021-03-08 13:57:33');



DROP TABLE IF EXISTS coupon_categories;

CREATE TABLE `coupon_categories` (
  `coupon_id` bigint(20) unsigned NOT NULL,
  `category_id` bigint(20) unsigned NOT NULL,
  `exclude` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`coupon_id`,`category_id`,`exclude`),
  KEY `coupon_categories_category_id_foreign` (`category_id`),
  CONSTRAINT `coupon_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE,
  CONSTRAINT `coupon_categories_coupon_id_foreign` FOREIGN KEY (`coupon_id`) REFERENCES `coupons` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS coupon_products;

CREATE TABLE `coupon_products` (
  `coupon_id` bigint(20) unsigned NOT NULL,
  `product_id` bigint(20) unsigned NOT NULL,
  `exclude` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`coupon_id`,`product_id`,`exclude`),
  KEY `coupon_products_product_id_foreign` (`product_id`),
  CONSTRAINT `coupon_products_coupon_id_foreign` FOREIGN KEY (`coupon_id`) REFERENCES `coupons` (`id`) ON DELETE CASCADE,
  CONSTRAINT `coupon_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS coupon_translations;

CREATE TABLE `coupon_translations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `coupon_id` bigint(20) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `coupon_translations_coupon_id_locale_unique` (`coupon_id`,`locale`),
  CONSTRAINT `coupon_translations_coupon_id_foreign` FOREIGN KEY (`coupon_id`) REFERENCES `coupons` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO coupon_translations VALUES('1','1','English','Simple Coupon');
INSERT INTO coupon_translations VALUES('2','1','language','Simple Coupon');



DROP TABLE IF EXISTS coupons;

CREATE TABLE `coupons` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` decimal(10,2) unsigned DEFAULT NULL,
  `is_percent` tinyint(1) NOT NULL,
  `free_shipping` tinyint(1) NOT NULL,
  `minimum_spend` decimal(10,2) unsigned DEFAULT NULL,
  `maximum_spend` decimal(10,2) unsigned DEFAULT NULL,
  `usage_limit_per_coupon` int(10) unsigned DEFAULT NULL,
  `usage_limit_per_customer` int(10) unsigned DEFAULT NULL,
  `used` int(11) NOT NULL DEFAULT 0,
  `is_active` tinyint(1) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `coupons_code_index` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO coupons VALUES('1','TEST','10.00','1','0','','','10','1','1','1','2020-11-01','2021-02-15','2020-11-04 16:29:33','2021-01-18 15:51:52');



DROP TABLE IF EXISTS currency;

CREATE TABLE `currency` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `base_currency` tinyint(4) NOT NULL,
  `exchange_rate` decimal(10,2) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO currency VALUES('1','USD','1','1.00','1','2020-11-01 23:55:56','2020-11-01 23:55:56');
INSERT INTO currency VALUES('2','EUR','0','0.86','1','2020-11-01 23:56:12','2020-11-01 23:56:12');
INSERT INTO currency VALUES('3','NGN','0','360.00','1','2020-11-14 20:51:33','2020-11-14 20:51:33');



DROP TABLE IF EXISTS customer_addresses;

CREATE TABLE `customer_addresses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO customer_addresses VALUES('1','3','Jhon','BD','Dhaka','Dhaka','97/2, Green Road','1321','0','2020-11-09 14:32:22','2020-11-09 21:03:01');
INSERT INTO customer_addresses VALUES('3','1','Jamal','BD','Bogora','Dhaka','43/c,Indira road','1215','0','2020-11-10 19:44:28','2020-11-10 19:44:28');
INSERT INTO customer_addresses VALUES('11','3','Kennedy','BD','Bogora','Dhaka','Test Address','1254','0','2020-11-11 16:09:10','2020-11-11 16:09:10');
INSERT INTO customer_addresses VALUES('26','18','Kuddus','BD','Dhaka','Dhaka','Test Address','1215','1','2020-11-13 14:05:54','2020-11-13 14:05:54');
INSERT INTO customer_addresses VALUES('27','38','Francis','BD','Dhaka','Dhaka','Test Address','1215','0','2021-02-11 12:49:10','2021-02-11 12:49:10');



DROP TABLE IF EXISTS database_backups;

CREATE TABLE `database_backups` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS email_templates;

CREATE TABLE `email_templates` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO email_templates VALUES('1','welcome_email','Welcome to Ultra Store','<table role=\"presentation\" border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
<tbody>
<tr>
<td style=\"padding: 20px 0 30px 0;\">
<table style=\"border-collapse: collapse; border: 1px solid #cccccc;\" border=\"0\" width=\"600\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">
<tbody>
<tr>
<td style=\"padding: 40px 0 30px 0;\" align=\"center\" bgcolor=\"#1e1e2c\"><img style=\"display: block;\" src=\"http://trickycode.net/smart_cash/public/images/company-logo.png\" alt=\"Creating Email Magic.\" width=\"80\" height=\"80\" /></td>
</tr>
<tr>
<td style=\"padding: 40px 30px 40px 30px;\" bgcolor=\"#ffffff\">
<table style=\"border-collapse: collapse; height: 95px; width: 100%;\" border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
<tbody>
<tr style=\"height: 30px;\">
<td style=\"color: #153643; font-family: Arial, sans-serif; height: 30px;\">
<h1 style=\"font-size: 24px; margin: 0;\">Welcome to Ultra Store</h1>
</td>
</tr>
<tr style=\"height: 47px;\">
<td style=\"color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 24px; padding: 20px 0px 30px; height: 47px;\">
<p style=\"margin: 0;\">Hi {name},</p>
<p style=\"margin: 0;\">Your account is now ready to use. You can now login to your portal using your email and password.</p>
<p style=\"margin: 0;\">&nbsp;</p>
<p style=\"margin: 0;\">Thank You</p>
<p style=\"margin: 0;\">Ultra Store</p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td style=\"padding: 30px 30px;\" bgcolor=\"#1e1e2c\">
<table style=\"border-collapse: collapse; width: 99.4769%;\" border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
<tbody>
<tr>
<td style=\"color: #ffffff; font-family: Arial, sans-serif; font-size: 14px; width: 99.8141%;\">
<p style=\"margin: 0px; text-align: center;\">&reg; Tricky Code 2021</p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>','2021-01-30 07:49:21','2021-01-30 16:01:42');
INSERT INTO email_templates VALUES('2','order_placed','Your Order Placed Successfully','<table role=\"presentation\" border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
<tbody>
<tr>
<td style=\"padding: 20px 0 30px 0;\">
<table style=\"border-collapse: collapse; border: 1px solid #cccccc;\" border=\"0\" width=\"600\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">
<tbody>
<tr>
<td style=\"padding: 40px 0 30px 0;\" align=\"center\" bgcolor=\"#1e1e2c\"><img style=\"display: block;\" src=\"http://trickycode.net/smart_cash/public/images/company-logo.png\" alt=\"Creating Email Magic.\" width=\"80\" height=\"80\" /></td>
</tr>
<tr>
<td style=\"padding: 40px 30px 40px 30px;\" bgcolor=\"#ffffff\">
<table style=\"border-collapse: collapse; height: 95px; width: 100%;\" border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
<tbody>
<tr style=\"height: 30px;\">
<td style=\"color: #153643; font-family: Arial, sans-serif; height: 30px;\">
<h1 style=\"font-size: 24px; margin: 0;\">Your Order Placed Successfully</h1>
</td>
</tr>
<tr style=\"height: 47px;\">
<td style=\"color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 24px; padding: 20px 0px 30px; height: 47px;\">
<p style=\"margin: 0;\">Hi {name},</p>
<p style=\"margin: 0;\">Your Order (Order ID: {order_id}) has been placed sucessfully. Your order will be shipped within next 3 business days.</p>
<p style=\"margin: 0;\">&nbsp;</p>
<p style=\"margin: 0;\">Order ID: {order_id}</p>
<p style=\"margin: 0;\">Order Status: {order_status}</p>
<p style=\"margin: 0;\">Payment Status: {payment_status}</p>
<p style=\"margin: 0;\">Payment Method: {payment_method}</p>
<p style=\"margin: 0;\">&nbsp;</p>
<p style=\"margin: 0;\">Thank you for shopping with us.</p>
<p style=\"margin: 0;\">&nbsp;</p>
<p style=\"margin: 0;\">Thank You</p>
<p style=\"margin: 0;\">Ultra Store</p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td style=\"padding: 30px 30px;\" bgcolor=\"#1e1e2c\">
<table style=\"border-collapse: collapse; width: 99.4769%;\" border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
<tbody>
<tr>
<td style=\"color: #ffffff; font-family: Arial, sans-serif; font-size: 14px; width: 99.8141%;\">
<p style=\"margin: 0px; text-align: center;\">&reg; Tricky Code 2021</p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>','2021-01-30 08:10:09','2021-02-11 06:31:48');
INSERT INTO email_templates VALUES('3','order_processing','Your Order Marked as Processing','<table role=\"presentation\" border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
<tbody>
<tr>
<td style=\"padding: 20px 0 30px 0;\">
<table style=\"border-collapse: collapse; border: 1px solid #cccccc;\" border=\"0\" width=\"600\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">
<tbody>
<tr>
<td style=\"padding: 40px 0 30px 0;\" align=\"center\" bgcolor=\"#1e1e2c\"><img style=\"display: block;\" src=\"http://trickycode.net/smart_cash/public/images/company-logo.png\" alt=\"Creating Email Magic.\" width=\"80\" height=\"80\" /></td>
</tr>
<tr>
<td style=\"padding: 40px 30px 40px 30px;\" bgcolor=\"#ffffff\">
<table style=\"border-collapse: collapse; height: 95px; width: 100%;\" border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
<tbody>
<tr style=\"height: 30px;\">
<td style=\"color: #153643; font-family: Arial, sans-serif; height: 30px;\">
<h1 style=\"font-size: 24px; margin: 0;\">Your Order has been processed</h1>
</td>
</tr>
<tr style=\"height: 47px;\">
<td style=\"color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 24px; padding: 20px 0px 30px; height: 47px;\">
<p style=\"margin: 0;\">Hi {name},</p>
<p style=\"margin: 0;\">Your Order (Order ID: {order_id}) has been processed.</p>
<p style=\"margin: 0;\">&nbsp;</p>
<p style=\"margin: 0;\">Order ID: {order_id}</p>
<p style=\"margin: 0;\">Order Status: {order_status}</p>
<p style=\"margin: 0;\">Payment Status: {payment_status}</p>
<p style=\"margin: 0;\">Payment Method: {payment_method}</p>
<p style=\"margin: 0;\">&nbsp;</p>
<p style=\"margin: 0;\">Thank you for shopping with us.</p>
<p style=\"margin: 0;\">&nbsp;</p>
<p style=\"margin: 0;\">Thank You</p>
<p style=\"margin: 0;\">Ultra Store</p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td style=\"padding: 30px 30px;\" bgcolor=\"#1e1e2c\">
<table style=\"border-collapse: collapse; width: 99.4769%;\" border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
<tbody>
<tr>
<td style=\"color: #ffffff; font-family: Arial, sans-serif; font-size: 14px; width: 99.8141%;\">
<p style=\"margin: 0px; text-align: center;\">&reg; Tricky Code 2021</p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>','2021-01-30 08:11:42','2021-02-11 07:49:22');
INSERT INTO email_templates VALUES('4','order_completed','Your Order Marked as Completed','<table role=\"presentation\" border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
<tbody>
<tr>
<td style=\"padding: 20px 0 30px 0;\">
<table style=\"border-collapse: collapse; border: 1px solid #cccccc;\" border=\"0\" width=\"600\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">
<tbody>
<tr>
<td style=\"padding: 40px 0 30px 0;\" align=\"center\" bgcolor=\"#1e1e2c\"><img style=\"display: block;\" src=\"http://trickycode.net/smart_cash/public/images/company-logo.png\" alt=\"Creating Email Magic.\" width=\"80\" height=\"80\" /></td>
</tr>
<tr>
<td style=\"padding: 40px 30px 40px 30px;\" bgcolor=\"#ffffff\">
<table style=\"border-collapse: collapse; height: 95px; width: 100%;\" border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
<tbody>
<tr style=\"height: 30px;\">
<td style=\"color: #153643; font-family: Arial, sans-serif; height: 30px;\">
<h1 style=\"font-size: 24px; margin: 0;\">Your Order has Completed</h1>
</td>
</tr>
<tr style=\"height: 47px;\">
<td style=\"color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 24px; padding: 20px 0px 30px; height: 47px;\">
<p style=\"margin: 0;\">Hi {name},</p>
<p style=\"margin: 0;\">Your Order (Order ID: {order_id}) has completed.</p>
<p style=\"margin: 0;\">&nbsp;</p>
<p style=\"margin: 0;\">Order ID: {order_id}</p>
<p style=\"margin: 0;\">Order Status: {order_status}</p>
<p style=\"margin: 0;\">Payment Status: {payment_status}</p>
<p style=\"margin: 0;\">Payment Method: {payment_method}</p>
<p style=\"margin: 0;\">&nbsp;</p>
<p style=\"margin: 0;\">Thank you for shopping with us.</p>
<p style=\"margin: 0;\">&nbsp;</p>
<p style=\"margin: 0;\">Thank You</p>
<p style=\"margin: 0;\">Ultra Store</p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td style=\"padding: 30px 30px;\" bgcolor=\"#1e1e2c\">
<table style=\"border-collapse: collapse; width: 99.4769%;\" border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
<tbody>
<tr>
<td style=\"color: #ffffff; font-family: Arial, sans-serif; font-size: 14px; width: 99.8141%;\">
<p style=\"margin: 0px; text-align: center;\">&reg; Tricky Code 2021</p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>','2021-01-30 08:12:11','2021-02-11 07:50:03');
INSERT INTO email_templates VALUES('5','order_canceled','Your Order Marked as Canceled','<table role=\"presentation\" border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
<tbody>
<tr>
<td style=\"padding: 20px 0 30px 0;\">
<table style=\"border-collapse: collapse; border: 1px solid #cccccc;\" border=\"0\" width=\"600\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">
<tbody>
<tr>
<td style=\"padding: 40px 0 30px 0;\" align=\"center\" bgcolor=\"#1e1e2c\"><img style=\"display: block;\" src=\"http://trickycode.net/smart_cash/public/images/company-logo.png\" alt=\"Creating Email Magic.\" width=\"80\" height=\"80\" /></td>
</tr>
<tr>
<td style=\"padding: 40px 30px 40px 30px;\" bgcolor=\"#ffffff\">
<table style=\"border-collapse: collapse; height: 95px; width: 100%;\" border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
<tbody>
<tr style=\"height: 30px;\">
<td style=\"color: #153643; font-family: Arial, sans-serif; height: 30px;\">
<h1 style=\"font-size: 24px; margin: 0;\">Your Order has been canceled</h1>
</td>
</tr>
<tr style=\"height: 47px;\">
<td style=\"color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 24px; padding: 20px 0px 30px; height: 47px;\">
<p style=\"margin: 0;\">Hi {name},</p>
<p style=\"margin: 0;\">Your Order (Order ID: {order_id}) has been canceled.</p>
<p style=\"margin: 0;\">&nbsp;</p>
<p style=\"margin: 0;\">Order ID: {order_id}</p>
<p style=\"margin: 0;\">Order Status: {order_status}</p>
<p style=\"margin: 0;\">Payment Status: {payment_status}</p>
<p style=\"margin: 0;\">Payment Method: {payment_method}</p>
<p style=\"margin: 0;\">&nbsp;</p>
<p style=\"margin: 0;\">Thank you for shopping with us.</p>
<p style=\"margin: 0;\">&nbsp;</p>
<p style=\"margin: 0;\">Thank You</p>
<p style=\"margin: 0;\">Ultra Store</p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td style=\"padding: 30px 30px;\" bgcolor=\"#1e1e2c\">
<table style=\"border-collapse: collapse; width: 99.4769%;\" border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
<tbody>
<tr>
<td style=\"color: #ffffff; font-family: Arial, sans-serif; font-size: 14px; width: 99.8141%;\">
<p style=\"margin: 0px; text-align: center;\">&reg; Tricky Code 2021</p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>','2021-01-30 08:13:06','2021-02-11 07:50:37');
INSERT INTO email_templates VALUES('6','order_on_hold','Your Order Marked as On Hold','<table role=\"presentation\" border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
<tbody>
<tr>
<td style=\"padding: 20px 0 30px 0;\">
<table style=\"border-collapse: collapse; border: 1px solid #cccccc;\" border=\"0\" width=\"600\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">
<tbody>
<tr>
<td style=\"padding: 40px 0 30px 0;\" align=\"center\" bgcolor=\"#1e1e2c\"><img style=\"display: block;\" src=\"http://trickycode.net/smart_cash/public/images/company-logo.png\" alt=\"Creating Email Magic.\" width=\"80\" height=\"80\" /></td>
</tr>
<tr>
<td style=\"padding: 40px 30px 40px 30px;\" bgcolor=\"#ffffff\">
<table style=\"border-collapse: collapse; height: 95px; width: 100%;\" border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
<tbody>
<tr style=\"height: 30px;\">
<td style=\"color: #153643; font-family: Arial, sans-serif; height: 30px;\">
<h1 style=\"font-size: 24px; margin: 0;\">Your Order Marked as On Hold</h1>
</td>
</tr>
<tr style=\"height: 47px;\">
<td style=\"color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 24px; padding: 20px 0px 30px; height: 47px;\">
<p style=\"margin: 0;\">Hi {name},</p>
<p style=\"margin: 0;\">Your Order (Order ID: {order_id}) has been hold.</p>
<p style=\"margin: 0;\">&nbsp;</p>
<p style=\"margin: 0;\">Order ID: {order_id}</p>
<p style=\"margin: 0;\">Order Status: {order_status}</p>
<p style=\"margin: 0;\">Payment Status: {payment_status}</p>
<p style=\"margin: 0;\">Payment Method: {payment_method}</p>
<p style=\"margin: 0;\">&nbsp;</p>
<p style=\"margin: 0;\">Thank you for shopping with us.</p>
<p style=\"margin: 0;\">&nbsp;</p>
<p style=\"margin: 0;\">Thank You</p>
<p style=\"margin: 0;\">Ultra Store</p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td style=\"padding: 30px 30px;\" bgcolor=\"#1e1e2c\">
<table style=\"border-collapse: collapse; width: 99.4769%;\" border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
<tbody>
<tr>
<td style=\"color: #ffffff; font-family: Arial, sans-serif; font-size: 14px; width: 99.8141%;\">
<p style=\"margin: 0px; text-align: center;\">&reg; Tricky Code 2021</p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>','2021-01-30 08:14:25','2021-02-11 07:51:38');
INSERT INTO email_templates VALUES('7','order_refunded','Money Refunded','<p>Your Order Money has been refunded.</p>','2021-01-30 08:15:25','2021-01-30 08:15:25');



DROP TABLE IF EXISTS entity_files;

CREATE TABLE `entity_files` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `media_id` bigint(20) unsigned NOT NULL,
  `entity_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `entity_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `entity_files_entity_type_entity_id_index` (`entity_type`,`entity_id`),
  KEY `entity_files_media_id_index` (`media_id`),
  KEY `entity_files_name_index` (`name`),
  CONSTRAINT `entity_files_media_id_foreign` FOREIGN KEY (`media_id`) REFERENCES `media` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO entity_files VALUES('1','12','App\\Entity\\Brand\\Brand','1','logo','2020-11-02 22:21:51','2020-11-02 22:21:51');
INSERT INTO entity_files VALUES('2','11','App\\Entity\\Brand\\Brand','2','logo','2020-11-02 22:22:00','2020-11-02 22:22:00');
INSERT INTO entity_files VALUES('3','10','App\\Entity\\Brand\\Brand','3','logo','2020-11-02 22:22:11','2020-11-02 22:22:11');
INSERT INTO entity_files VALUES('4','9','App\\Entity\\Brand\\Brand','4','logo','2020-11-02 22:22:19','2020-11-02 22:22:19');
INSERT INTO entity_files VALUES('5','8','App\\Entity\\Brand\\Brand','5','logo','2020-11-02 22:22:29','2020-11-02 22:22:29');
INSERT INTO entity_files VALUES('6','7','App\\Entity\\Brand\\Brand','6','logo','2020-11-02 22:22:37','2020-11-02 22:22:37');
INSERT INTO entity_files VALUES('7','6','App\\Entity\\Brand\\Brand','7','logo','2020-11-02 22:22:51','2020-11-02 22:22:51');
INSERT INTO entity_files VALUES('8','5','App\\Entity\\Brand\\Brand','8','logo','2020-11-02 22:23:01','2020-11-02 22:23:01');
INSERT INTO entity_files VALUES('9','4','App\\Entity\\Brand\\Brand','9','logo','2020-11-02 22:23:08','2020-11-02 22:23:08');
INSERT INTO entity_files VALUES('10','3','App\\Entity\\Brand\\Brand','10','logo','2020-11-02 22:23:16','2020-11-02 22:23:16');
INSERT INTO entity_files VALUES('11','2','App\\Entity\\Brand\\Brand','11','logo','2020-11-02 22:23:29','2020-11-02 22:23:29');
INSERT INTO entity_files VALUES('12','1','App\\Entity\\Brand\\Brand','12','logo','2020-11-02 22:23:39','2020-11-02 22:23:39');
INSERT INTO entity_files VALUES('58','15','App\\Entity\\Product\\Product','3','product_image','2021-03-06 14:26:23','2021-03-06 14:26:23');
INSERT INTO entity_files VALUES('59','14','App\\Entity\\Product\\Product','2','product_image','2021-03-06 14:30:29','2021-03-06 14:30:29');
INSERT INTO entity_files VALUES('60','14','App\\Entity\\Product\\Product','2','gallery_images','2021-03-06 14:30:29','2021-03-06 14:30:29');
INSERT INTO entity_files VALUES('61','13','App\\Entity\\Product\\Product','2','gallery_images','2021-03-06 14:30:29','2021-03-06 14:30:29');
INSERT INTO entity_files VALUES('62','15','App\\Entity\\Product\\Product','2','gallery_images','2021-03-06 14:30:29','2021-03-06 14:30:29');
INSERT INTO entity_files VALUES('65','13','App\\Entity\\Product\\Product','1','product_image','2021-03-12 14:22:39','2021-03-12 14:22:39');



DROP TABLE IF EXISTS failed_jobs;

CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS media;

CREATE TABLE `media` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_extension` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO media VALUES('1','apple.png','media/hRv1IuQURPkpyvAjGlsenzO1O9w44uPDje3VKbxL.png','image/png','27158','png','1','2020-11-02 22:21:36','2020-11-02 22:21:36');
INSERT INTO media VALUES('2','adidas.png','media/NWYXSxrtY5j6gNkjzLfCOTG3l8EPy5iAKm5nqBKB.png','image/png','27354','png','1','2020-11-02 22:21:36','2020-11-02 22:21:36');
INSERT INTO media VALUES('3','dell.png','media/DgdUyV0aKf3jQRGGuJKvkWuThE3x6HrRBp7begI3.png','image/png','26584','png','1','2020-11-02 22:21:36','2020-11-02 22:21:36');
INSERT INTO media VALUES('4','hp.png','media/Keg8pDxHENiDYZQV9k9xcKdpfVjNVAidI0eK6iwy.png','image/png','26480','png','1','2020-11-02 22:21:36','2020-11-02 22:21:36');
INSERT INTO media VALUES('5','lg.png','media/Qt2ZoyyGUFuNtUkC7vnTSNHtOKcpmGEdDg9RO9Bu.png','image/png','26405','png','1','2020-11-02 22:21:37','2020-11-02 22:21:37');
INSERT INTO media VALUES('6','microsoft.png','media/j16nCTjqTSNCXVG55oIuBPTUZvA84JzI3mskIIIz.png','image/png','27238','png','1','2020-11-02 22:21:37','2020-11-02 22:21:37');
INSERT INTO media VALUES('7','nike.png','media/VT5jwFtRVqjXGIt2ltUMueRGN51NqaGtDU3S3DLR.png','image/png','28826','png','1','2020-11-02 22:21:37','2020-11-02 22:21:37');
INSERT INTO media VALUES('8','omega.png','media/OxNQsqHH5HDGtGJP3SLkM43LWHZXKalBjEZRcA7g.png','image/png','28859','png','1','2020-11-02 22:21:37','2020-11-02 22:21:37');
INSERT INTO media VALUES('9','puma.png','media/PlceSBoiZ8c5vPH7W5Fm7FKRgtjo7blQjKWAMRgg.png','image/png','26250','png','1','2020-11-02 22:21:38','2020-11-02 22:21:38');
INSERT INTO media VALUES('10','rolex.png','media/ijtN8nhIAyFBYmCwRHicnbEVFOPTwZgwBCeCFe9C.png','image/png','27461','png','1','2020-11-02 22:21:38','2020-11-02 22:21:38');
INSERT INTO media VALUES('11','samsung.png','media/TkC1GuUkejIOpiCM2T9TQUuFElr5Ky3o20Jx5dGl.png','image/png','29186','png','1','2020-11-02 22:21:38','2020-11-02 22:21:38');
INSERT INTO media VALUES('12','sony.png','media/mV02j93Th2fJ7YaD9FbrjVvhS9SX26DfM1QFTdwx.png','image/png','35110','png','1','2020-11-02 22:21:38','2020-11-02 22:21:38');
INSERT INTO media VALUES('13','Apple-MacBook-15-inch.jpg','media/SllIFCW6qRXhP3UeHbYWT70Pmy4Rwoe3Im5y8ud8.jpeg','image/jpeg','53103','jpeg','1','2020-11-03 14:25:14','2020-11-03 14:25:14');
INSERT INTO media VALUES('14','iphone-12.jpg','media/XKq8PEbA9mgqB2tv64RRa97ZhjFUr99zHsoCm5x5.jpeg','image/jpeg','30244','jpeg','1','2020-11-03 14:47:21','2020-11-03 14:47:21');
INSERT INTO media VALUES('15','1041px-Adobe_Photoshop_CS6_icon.svg.png','media/4altjojlA0uxGduJ63ts52ZL7k74NO3TfvlP6yQ9.png','image/png','52551','png','1','2020-11-03 19:06:05','2020-11-03 19:06:05');



DROP TABLE IF EXISTS meta_data;

CREATE TABLE `meta_data` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `entity_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `entity_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `meta_data_entity_type_entity_id_index` (`entity_type`,`entity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO meta_data VALUES('1','App\\Entity\\Product\\Product','1','2020-11-03 14:42:22','2020-11-03 14:42:22');
INSERT INTO meta_data VALUES('2','App\\Entity\\Product\\Product','2','2020-11-03 14:47:50','2020-11-03 14:47:50');
INSERT INTO meta_data VALUES('3','App\\Entity\\Product\\Product','3','2020-11-03 19:06:11','2020-11-03 19:06:11');
INSERT INTO meta_data VALUES('4','App\\Entity\\Product\\Product','4','2021-03-12 16:37:49','2021-03-12 16:37:49');
INSERT INTO meta_data VALUES('5','App\\Entity\\Page\\Page','1','2021-03-15 16:28:46','2021-03-15 16:28:46');
INSERT INTO meta_data VALUES('6','App\\Entity\\Page\\Page','2','2021-03-15 16:29:26','2021-03-15 16:29:26');
INSERT INTO meta_data VALUES('7','App\\Entity\\Page\\Page','3','2021-03-15 22:14:50','2021-03-15 22:14:50');
INSERT INTO meta_data VALUES('8','App\\Entity\\Page\\Page','4','2021-03-15 22:15:05','2021-03-15 22:15:05');



DROP TABLE IF EXISTS meta_data_translations;

CREATE TABLE `meta_data_translations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `meta_data_id` bigint(20) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `meta_data_translations_meta_data_id_locale_unique` (`meta_data_id`,`locale`),
  CONSTRAINT `meta_data_translations_meta_data_id_foreign` FOREIGN KEY (`meta_data_id`) REFERENCES `meta_data` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO meta_data_translations VALUES('1','1','English','','','');
INSERT INTO meta_data_translations VALUES('2','2','English','Iphone 12 128GB','iphone','This is iphone 12 descriptions');
INSERT INTO meta_data_translations VALUES('3','3','English','','','');
INSERT INTO meta_data_translations VALUES('4','4','English','','','');
INSERT INTO meta_data_translations VALUES('5','5','English','','','');
INSERT INTO meta_data_translations VALUES('6','6','English','','','');
INSERT INTO meta_data_translations VALUES('7','7','English','','','');
INSERT INTO meta_data_translations VALUES('8','8','English','','','');



DROP TABLE IF EXISTS migrations;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO migrations VALUES('1','2014_10_12_000000_create_users_table','1');
INSERT INTO migrations VALUES('2','2014_10_12_100000_create_password_resets_table','1');
INSERT INTO migrations VALUES('3','2018_11_12_152015_create_email_templates_table','1');
INSERT INTO migrations VALUES('4','2019_08_19_000000_create_failed_jobs_table','1');
INSERT INTO migrations VALUES('5','2019_09_01_080940_create_settings_table','1');
INSERT INTO migrations VALUES('6','2020_07_02_145857_create_database_backups_table','1');
INSERT INTO migrations VALUES('7','2020_07_06_142817_create_roles_table','1');
INSERT INTO migrations VALUES('8','2020_07_06_143240_create_permissions_table','1');
INSERT INTO migrations VALUES('9','2020_07_25_061549_create_currency_table','1');
INSERT INTO migrations VALUES('10','2020_07_29_095329_create_tax_classes_table','1');
INSERT INTO migrations VALUES('11','2020_07_29_095340_create_tax_classes_translation_table','1');
INSERT INTO migrations VALUES('12','2020_07_29_095348_create_tax_rates_table','1');
INSERT INTO migrations VALUES('13','2020_07_29_095357_create_tax_rates_translation_table','1');
INSERT INTO migrations VALUES('14','2020_07_30_074942_create_media_table','1');
INSERT INTO migrations VALUES('15','2020_07_30_152834_create_tags_table','1');
INSERT INTO migrations VALUES('16','2020_07_30_153031_create_tags_translation_table','1');
INSERT INTO migrations VALUES('17','2020_07_31_135138_create_brands_table','1');
INSERT INTO migrations VALUES('18','2020_07_31_140257_create_brands_translation_table','1');
INSERT INTO migrations VALUES('19','2020_07_31_145819_create_entity_files_table','1');
INSERT INTO migrations VALUES('20','2020_08_11_135105_create_category_table','1');
INSERT INTO migrations VALUES('21','2020_08_11_135531_create_category_translation_table','1');
INSERT INTO migrations VALUES('22','2020_08_23_160650_create_products_table','1');
INSERT INTO migrations VALUES('23','2020_08_23_161219_create_product_translations_table','1');
INSERT INTO migrations VALUES('24','2020_08_23_163548_create_product_categories_table','1');
INSERT INTO migrations VALUES('25','2020_08_23_163600_create_product_tags_table','1');
INSERT INTO migrations VALUES('26','2020_08_24_152430_create_product_variations_table','1');
INSERT INTO migrations VALUES('27','2020_08_24_152831_create_product_variation_items_table','1');
INSERT INTO migrations VALUES('28','2020_08_24_171314_create_product_variation_prices_table','1');
INSERT INTO migrations VALUES('29','2020_08_24_171315_create_coupons_table','1');
INSERT INTO migrations VALUES('30','2020_08_24_171316_create_coupon_translations_table','1');
INSERT INTO migrations VALUES('31','2020_08_24_171317_create_coupon_products_table','1');
INSERT INTO migrations VALUES('32','2020_08_24_171318_create_coupon_categories_table','1');
INSERT INTO migrations VALUES('33','2020_08_24_171319_create_meta_data_table','1');
INSERT INTO migrations VALUES('34','2020_08_24_171320_create_meta_data_translations_table','1');
INSERT INTO migrations VALUES('35','2020_09_02_145504_create_pages_table','1');
INSERT INTO migrations VALUES('36','2020_09_02_145952_create_page_translations_table','1');
INSERT INTO migrations VALUES('37','2020_09_04_084255_create_navigations_table','1');
INSERT INTO migrations VALUES('38','2020_09_04_084515_create_navigation_items_table','1');
INSERT INTO migrations VALUES('39','2020_09_04_084719_create_navigation_item_translations_table','1');
INSERT INTO migrations VALUES('40','2020_11_08_153213_create_customer_addresses_table','2');
INSERT INTO migrations VALUES('45','2020_11_11_172141_create_orders_table','3');
INSERT INTO migrations VALUES('46','2020_11_11_172303_create_order_products_table','3');
INSERT INTO migrations VALUES('47','2020_11_11_172521_create_order_taxes_table','3');
INSERT INTO migrations VALUES('48','2020_11_13_142034_create_transactions_table','3');
INSERT INTO migrations VALUES('50','2021_01_22_171533_create_wish_lists_table','4');
INSERT INTO migrations VALUES('55','2021_01_24_064834_create_product_comments_table','5');
INSERT INTO migrations VALUES('56','2021_01_27_104627_create_product_reviews_table','5');



DROP TABLE IF EXISTS navigation_item_translations;

CREATE TABLE `navigation_item_translations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `navigation_item_id` bigint(20) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `navigation_item_translations_navigation_item_id_locale_unique` (`navigation_item_id`,`locale`),
  CONSTRAINT `navigation_item_translations_navigation_item_id_foreign` FOREIGN KEY (`navigation_item_id`) REFERENCES `navigation_items` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO navigation_item_translations VALUES('1','1','English','Home','2021-03-15 21:58:36','2021-03-15 21:58:36');
INSERT INTO navigation_item_translations VALUES('2','2','English','Shop','2021-03-15 21:59:23','2021-03-15 21:59:23');
INSERT INTO navigation_item_translations VALUES('3','3','English','Brands','2021-03-15 21:59:37','2021-03-15 21:59:37');
INSERT INTO navigation_item_translations VALUES('4','4','English','About Us','2021-03-15 21:59:59','2021-03-15 21:59:59');
INSERT INTO navigation_item_translations VALUES('5','5','English','Contact Us','2021-03-15 22:00:13','2021-03-15 22:00:13');
INSERT INTO navigation_item_translations VALUES('6','6','English','Sub Page 1','2021-03-15 22:15:30','2021-03-15 22:15:30');
INSERT INTO navigation_item_translations VALUES('7','7','English','Sub Page 2','2021-03-15 22:15:51','2021-03-15 22:15:51');
INSERT INTO navigation_item_translations VALUES('9','9','English','Electronic Devices','2021-03-17 15:34:06','2021-03-17 15:34:06');
INSERT INTO navigation_item_translations VALUES('10','10','English','Mobile','2021-03-17 15:37:14','2021-03-17 15:37:14');
INSERT INTO navigation_item_translations VALUES('11','11','English','Laptop','2021-03-17 15:37:30','2021-03-17 15:37:30');
INSERT INTO navigation_item_translations VALUES('12','12','English','Cameras','2021-03-17 15:37:44','2021-03-17 15:37:44');
INSERT INTO navigation_item_translations VALUES('13','13','English','Tablets','2021-03-17 15:37:58','2021-03-17 15:37:58');
INSERT INTO navigation_item_translations VALUES('14','14','English','Desktop Components','2021-03-17 15:38:48','2021-03-17 15:38:48');
INSERT INTO navigation_item_translations VALUES('15','15','English','Motherboard','2021-03-17 15:39:09','2021-03-17 15:39:09');
INSERT INTO navigation_item_translations VALUES('16','16','English','Health & Beauty','2021-03-17 15:40:01','2021-03-17 15:40:01');
INSERT INTO navigation_item_translations VALUES('17','17','English','Hair Care','2021-03-17 15:41:06','2021-03-17 15:41:06');
INSERT INTO navigation_item_translations VALUES('18','18','English','Skin Care','2021-03-17 15:47:26','2021-03-17 15:47:26');
INSERT INTO navigation_item_translations VALUES('19','19','English','Food Supplements','2021-03-17 15:47:54','2021-03-17 15:47:54');
INSERT INTO navigation_item_translations VALUES('20','20','English','Men\'s Fashion','2021-03-17 15:48:19','2021-03-17 15:48:19');
INSERT INTO navigation_item_translations VALUES('21','21','English','T-Shirts','2021-03-17 15:48:49','2021-03-17 15:48:49');
INSERT INTO navigation_item_translations VALUES('22','22','English','Shirts','2021-03-17 15:49:01','2021-03-17 15:49:01');
INSERT INTO navigation_item_translations VALUES('23','23','English','Jeans','2021-03-17 15:49:13','2021-03-17 15:49:13');
INSERT INTO navigation_item_translations VALUES('24','24','English','Women\'s Fashion','2021-03-17 16:04:48','2021-03-17 16:04:48');
INSERT INTO navigation_item_translations VALUES('25','25','English','Sports & Outdoor','2021-03-17 16:05:12','2021-03-17 16:05:12');
INSERT INTO navigation_item_translations VALUES('26','26','English','Software','2021-03-17 16:05:26','2021-03-17 16:05:26');
INSERT INTO navigation_item_translations VALUES('27','27','English','Women\'s Bags','2021-03-17 16:06:36','2021-03-17 16:06:36');
INSERT INTO navigation_item_translations VALUES('28','28','English','Women\'s Shoes','2021-03-17 16:06:57','2021-03-17 16:06:57');
INSERT INTO navigation_item_translations VALUES('29','29','English','Kurti','2021-03-17 16:07:13','2021-03-17 16:07:13');



DROP TABLE IF EXISTS navigation_items;

CREATE TABLE `navigation_items` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `navigation_id` bigint(20) unsigned NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_id` bigint(20) unsigned DEFAULT NULL,
  `category_id` bigint(20) unsigned DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` bigint(20) unsigned DEFAULT NULL,
  `position` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `css_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `css_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `navigation_items_parent_id_foreign` (`parent_id`),
  KEY `navigation_items_category_id_foreign` (`category_id`),
  KEY `navigation_items_page_id_foreign` (`page_id`),
  KEY `navigation_items_navigation_id_index` (`navigation_id`),
  CONSTRAINT `navigation_items_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE,
  CONSTRAINT `navigation_items_navigation_id_foreign` FOREIGN KEY (`navigation_id`) REFERENCES `navigations` (`id`) ON DELETE CASCADE,
  CONSTRAINT `navigation_items_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE,
  CONSTRAINT `navigation_items_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `navigation_items` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO navigation_items VALUES('1','1','dynamic_url','','','/','<i class=\"ti-home\"></i>','_self','','1','1','','','2021-03-15 21:58:36','2021-03-17 13:44:18');
INSERT INTO navigation_items VALUES('2','1','dynamic_url','','','/shop','<i class=\"ti-shopping-cart\"></i>','_self','','2','1','','','2021-03-15 21:59:23','2021-03-17 13:51:45');
INSERT INTO navigation_items VALUES('3','1','dynamic_url','','','/brands','<i class=\"ti-apple\"></i>','_self','','3','1','','','2021-03-15 21:59:37','2021-03-17 13:52:58');
INSERT INTO navigation_items VALUES('4','1','page','1','','','<i class=\"ti-user\"></i>','_self','','4','1','','','2021-03-15 21:59:59','2021-03-17 13:53:24');
INSERT INTO navigation_items VALUES('5','1','page','2','','','<i class=\"ti-email\"></i>','_self','','5','1','','','2021-03-15 22:00:13','2021-03-17 13:54:08');
INSERT INTO navigation_items VALUES('6','1','page','3','','','','_self','4','5','1','','','2021-03-15 22:15:30','2021-03-15 22:15:39');
INSERT INTO navigation_items VALUES('7','1','page','4','','','','_self','4','6','1','','','2021-03-15 22:15:51','2021-03-17 13:50:36');
INSERT INTO navigation_items VALUES('9','2','category','','1','','','_self','','1','1','','','2021-03-17 15:34:06','2021-03-17 15:38:56');
INSERT INTO navigation_items VALUES('10','2','category','','7','','','_self','9','2','1','','','2021-03-17 15:37:14','2021-03-17 15:38:56');
INSERT INTO navigation_items VALUES('11','2','category','','8','','','_self','9','3','1','','','2021-03-17 15:37:30','2021-03-17 15:38:56');
INSERT INTO navigation_items VALUES('12','2','category','','9','','','_self','9','4','1','','','2021-03-17 15:37:44','2021-03-17 15:38:56');
INSERT INTO navigation_items VALUES('13','2','category','','10','','','_self','9','5','1','','','2021-03-17 15:37:58','2021-03-17 15:38:56');
INSERT INTO navigation_items VALUES('14','2','category','','2','','','_self','','2','1','','','2021-03-17 15:38:48','2021-03-17 15:38:56');
INSERT INTO navigation_items VALUES('15','2','category','','25','','','_self','14','3','1','','','2021-03-17 15:39:09','2021-03-17 15:39:17');
INSERT INTO navigation_items VALUES('16','2','category','','3','','','_self','','3','1','','','2021-03-17 15:40:01','2021-03-17 15:40:13');
INSERT INTO navigation_items VALUES('17','2','category','','11','','','_self','16','4','1','','','2021-03-17 15:41:06','2021-03-17 15:47:08');
INSERT INTO navigation_items VALUES('18','2','category','','12','','','_self','16','5','1','','','2021-03-17 15:47:26','2021-03-17 15:47:34');
INSERT INTO navigation_items VALUES('19','2','category','','13','','','_self','16','6','1','','','2021-03-17 15:47:54','2021-03-17 15:48:24');
INSERT INTO navigation_items VALUES('20','2','category','','4','','','_self','','4','1','','','2021-03-17 15:48:19','2021-03-17 15:48:24');
INSERT INTO navigation_items VALUES('21','2','category','','14','','','_self','20','5','1','','','2021-03-17 15:48:49','2021-03-17 15:49:23');
INSERT INTO navigation_items VALUES('22','2','category','','15','','','_self','20','6','1','','','2021-03-17 15:49:01','2021-03-17 15:49:24');
INSERT INTO navigation_items VALUES('23','2','category','','16','','','_self','20','7','1','','','2021-03-17 15:49:13','2021-03-17 15:49:24');
INSERT INTO navigation_items VALUES('24','2','category','','5','','','_self','','5','1','','','2021-03-17 16:04:48','2021-03-17 16:06:17');
INSERT INTO navigation_items VALUES('25','2','category','','6','','','_self','','6','1','','','2021-03-17 16:05:12','2021-03-17 16:06:17');
INSERT INTO navigation_items VALUES('26','2','category','','24','','','_self','','7','1','','','2021-03-17 16:05:26','2021-03-17 16:06:17');
INSERT INTO navigation_items VALUES('27','2','category','','18','','','_self','24','6','1','','','2021-03-17 16:06:36','2021-03-17 16:07:26');
INSERT INTO navigation_items VALUES('28','2','category','','19','','','_self','24','7','1','','','2021-03-17 16:06:57','2021-03-17 16:07:26');
INSERT INTO navigation_items VALUES('29','2','category','','20','','','_self','24','8','1','','','2021-03-17 16:07:13','2021-03-17 16:07:26');



DROP TABLE IF EXISTS navigations;

CREATE TABLE `navigations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO navigations VALUES('1','Primary Menu','1','2020-11-16 18:30:14','2021-03-17 12:54:42');
INSERT INTO navigations VALUES('2','Category Menu','1','2021-03-17 12:55:09','2021-03-17 12:55:09');



DROP TABLE IF EXISTS order_products;

CREATE TABLE `order_products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) unsigned NOT NULL,
  `product_id` bigint(20) unsigned NOT NULL,
  `product_attributes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit_price` decimal(10,2) unsigned NOT NULL,
  `qty` int(11) NOT NULL,
  `line_total` decimal(10,2) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_products_order_id_foreign` (`order_id`),
  CONSTRAINT `order_products_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO order_products VALUES('1','1','3','a:0:{}','300.00','1','300.00','2020-11-14 21:45:02','2020-11-14 21:45:02');
INSERT INTO order_products VALUES('2','2','2','a:0:{}','799.00','1','799.00','2020-11-16 17:19:12','2020-11-16 17:19:12');
INSERT INTO order_products VALUES('3','3','2','a:0:{}','799.00','1','799.00','2020-12-01 14:30:33','2020-12-01 14:30:33');
INSERT INTO order_products VALUES('15','15','2','a:0:{}','799.00','1','799.00','2021-02-11 12:49:38','2021-02-11 12:49:38');
INSERT INTO order_products VALUES('16','16','1','a:2:{s:11:\"Screen SIze\";s:7:\"13 Inch\";s:7:\"Storage\";s:5:\"256GB\";}','1200.00','1','1200.00','2021-02-11 13:03:32','2021-02-11 13:03:32');



DROP TABLE IF EXISTS order_taxes;

CREATE TABLE `order_taxes` (
  `order_id` bigint(20) unsigned NOT NULL,
  `tax_rate_id` bigint(20) unsigned NOT NULL,
  `amount` decimal(15,4) unsigned NOT NULL,
  PRIMARY KEY (`order_id`,`tax_rate_id`),
  KEY `order_taxes_tax_rate_id_foreign` (`tax_rate_id`),
  CONSTRAINT `order_taxes_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `order_taxes_tax_rate_id_foreign` FOREIGN KEY (`tax_rate_id`) REFERENCES `tax_rates` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO order_taxes VALUES('2','1','79.9000');
INSERT INTO order_taxes VALUES('2','2','39.9500');
INSERT INTO order_taxes VALUES('15','1','79.9000');
INSERT INTO order_taxes VALUES('15','2','39.9500');



DROP TABLE IF EXISTS orders;

CREATE TABLE `orders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` bigint(20) DEFAULT NULL,
  `customer_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_post_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_post_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_total` decimal(10,2) unsigned NOT NULL,
  `shipping_method` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_cost` decimal(10,2) unsigned NOT NULL,
  `coupon_id` bigint(20) DEFAULT NULL,
  `discount` decimal(10,2) unsigned NOT NULL,
  `total` decimal(10,2) unsigned NOT NULL,
  `payment_method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_rate` decimal(10,2) NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO orders VALUES('1','3','Demo Customer','customer@demo.com','16584564','Jhon','Dhaka','Dhaka','1321','BD','97/2, Green Road','Jhon','Dhaka','Dhaka','1321','BD','97/2, Green Road','300.00','Flat Rate','25.00','','0.00','325.00','','EUR','0.86','English','completed','','2020-11-14 21:45:02','2021-01-23 20:24:24');
INSERT INTO orders VALUES('2','3','Demo Customer','customer@demo.com','16584564','Jhon','Dhaka','Dhaka','1321','BD','97/2, Green Road','Jhon','Dhaka','Dhaka','1321','BD','97/2, Green Road','799.00','Flat Rate','25.00','','0.00','943.85','','USD','1.00','English','pending_payment','','2020-11-16 17:19:12','2020-11-16 22:54:34');
INSERT INTO orders VALUES('3','1','Admin','admin@demo.com','+674588448','Jamal','Dhaka','Bogora','1215','BD','43/c,Indira road','Jamal','Dhaka','Bogora','1215','BD','43/c,Indira road','799.00','Flat Rate','25.00','1','79.90','744.10','Bank_Transfer','USD','1.00','English','pending','','2020-12-01 14:30:33','2020-12-01 14:31:21');
INSERT INTO orders VALUES('15','38','Kennedy','kennedyrodrick93@gmail.com','01825644814','Francis','Dhaka','Dhaka','1215','BD','Test Address','Francis','Dhaka','Dhaka','1215','BD','Test Address','799.00','Flat Rate','25.00','','0.00','943.85','Cash_On_Delivery','USD','1.00','English','processing','','2021-02-11 12:49:38','2021-02-11 13:53:06');
INSERT INTO orders VALUES('16','','Guest User','guest@gmail.com','018151545454','Guest User','Gazipur','Gazipur','1215','BD','Test Address','Guest User','Gazipur','Gazipur','1215','BD','Test Address','1200.00','Free Shipping','0.00','','0.00','1200.00','PayPal','USD','1.00','English','pending','','2021-02-11 13:03:32','2021-02-11 13:04:17');



DROP TABLE IF EXISTS page_translations;

CREATE TABLE `page_translations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` bigint(20) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `page_translations_page_id_locale_unique` (`page_id`,`locale`),
  CONSTRAINT `page_translations_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO page_translations VALUES('1','1','English','About US','<h2>What is Lorem Ipsum?</h2>
<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>','2021-03-15 16:28:46','2021-03-15 16:28:46');
INSERT INTO page_translations VALUES('2','2','English','Contact Us','','2021-03-15 16:29:26','2021-03-15 16:29:26');
INSERT INTO page_translations VALUES('3','3','English','Sub Page 1','<p>This is sub page 1</p>','2021-03-15 22:14:50','2021-03-15 22:14:50');
INSERT INTO page_translations VALUES('4','4','English','Sub Page 2','<p>This is Sub Page 2</p>','2021-03-15 22:15:05','2021-03-15 22:15:05');



DROP TABLE IF EXISTS pages;

CREATE TABLE `pages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `template` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO pages VALUES('1','about-us','1','','2021-03-15 16:28:45','2021-03-15 16:28:45');
INSERT INTO pages VALUES('2','contact-us','1','contact','2021-03-15 16:29:26','2021-03-15 16:29:26');
INSERT INTO pages VALUES('3','sub-page-1','1','','2021-03-15 22:14:50','2021-03-15 22:14:50');
INSERT INTO pages VALUES('4','sub-page-2','1','','2021-03-15 22:15:05','2021-03-15 22:15:05');



DROP TABLE IF EXISTS password_resets;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO password_resets VALUES('customer@demo.com','$2y$10$yyYOJXWKGV0kNWKErmKUP.PEDunpD/rkVuIsPJbHYdoo7GA0GBaA2','2021-01-30 14:11:54');



DROP TABLE IF EXISTS permissions;

CREATE TABLE `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NOT NULL,
  `permission` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS product_categories;

CREATE TABLE `product_categories` (
  `product_id` bigint(20) unsigned NOT NULL,
  `category_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`product_id`,`category_id`),
  KEY `product_categories_category_id_foreign` (`category_id`),
  CONSTRAINT `product_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE,
  CONSTRAINT `product_categories_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO product_categories VALUES('1','1');
INSERT INTO product_categories VALUES('1','7');
INSERT INTO product_categories VALUES('1','8');
INSERT INTO product_categories VALUES('2','1');
INSERT INTO product_categories VALUES('2','7');
INSERT INTO product_categories VALUES('3','24');



DROP TABLE IF EXISTS product_comments;

CREATE TABLE `product_comments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `product_id` bigint(20) unsigned NOT NULL,
  `parent_id` bigint(20) unsigned DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_comments_user_id_foreign` (`user_id`),
  KEY `product_comments_product_id_foreign` (`product_id`),
  KEY `product_comments_parent_id_foreign` (`parent_id`),
  CONSTRAINT `product_comments_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `product_comments` (`id`) ON DELETE CASCADE,
  CONSTRAINT `product_comments_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  CONSTRAINT `product_comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO product_comments VALUES('9','1','3','','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.','2021-01-29 16:52:31','2021-01-29 16:52:31');



DROP TABLE IF EXISTS product_reviews;

CREATE TABLE `product_reviews` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `product_id` bigint(20) unsigned NOT NULL,
  `rating` bigint(20) NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_approved` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_reviews_user_id_foreign` (`user_id`),
  KEY `product_reviews_product_id_foreign` (`product_id`),
  CONSTRAINT `product_reviews_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  CONSTRAINT `product_reviews_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO product_reviews VALUES('3','1','3','4','This is 5 Star Rating','1','2021-01-29 23:37:17','2021-01-29 23:40:08');



DROP TABLE IF EXISTS product_tags;

CREATE TABLE `product_tags` (
  `product_id` bigint(20) unsigned NOT NULL,
  `tag_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`product_id`,`tag_id`),
  KEY `product_tags_tag_id_foreign` (`tag_id`),
  CONSTRAINT `product_tags_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  CONSTRAINT `product_tags_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO product_tags VALUES('1','1');
INSERT INTO product_tags VALUES('2','1');



DROP TABLE IF EXISTS product_translations;

CREATE TABLE `product_translations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_translations_product_id_locale_unique` (`product_id`,`locale`),
  CONSTRAINT `product_translations_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO product_translations VALUES('1','1','English','Macbook Pro','<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.','2020-11-03 14:42:22','2020-11-03 14:42:22');
INSERT INTO product_translations VALUES('2','2','English','Iphone 12 128GB','<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English.</p>
<p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>','It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English.','2020-11-03 14:47:50','2020-11-03 14:47:50');
INSERT INTO product_translations VALUES('3','3','English','Adobe Photoshop CS6','<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text.</p>
<p>All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.</p>
<p>It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>','There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text.','2020-11-03 19:06:11','2020-11-03 19:06:11');
INSERT INTO product_translations VALUES('4','4','English','Macbook Air','<p>Casio Watch</p>','Casio Watch','2021-03-12 16:37:49','2021-03-12 20:24:27');



DROP TABLE IF EXISTS product_variation_items;

CREATE TABLE `product_variation_items` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `variation_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_variation_items_variation_id_foreign` (`variation_id`),
  CONSTRAINT `product_variation_items_variation_id_foreign` FOREIGN KEY (`variation_id`) REFERENCES `product_variations` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO product_variation_items VALUES('65','33','13 Inch');
INSERT INTO product_variation_items VALUES('66','33','15 Inch');
INSERT INTO product_variation_items VALUES('67','34','256GB');
INSERT INTO product_variation_items VALUES('68','34','512GB');



DROP TABLE IF EXISTS product_variation_prices;

CREATE TABLE `product_variation_prices` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) unsigned NOT NULL,
  `option` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `special_price` decimal(10,2) unsigned DEFAULT NULL,
  `is_available` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_variation_prices_product_id_foreign` (`product_id`),
  CONSTRAINT `product_variation_prices_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO product_variation_prices VALUES('37','1','{\"33\":{\"id\":65,\"variation_id\":33,\"name\":\"13 Inch\"},\"34\":{\"id\":67,\"variation_id\":34,\"name\":\"256GB\"}}','1200.00','','1');
INSERT INTO product_variation_prices VALUES('38','1','{\"33\":{\"id\":65,\"variation_id\":33,\"name\":\"13 Inch\"},\"34\":{\"id\":68,\"variation_id\":34,\"name\":\"512GB\"}}','1500.00','','1');
INSERT INTO product_variation_prices VALUES('39','1','{\"33\":{\"id\":66,\"variation_id\":33,\"name\":\"15 Inch\"},\"34\":{\"id\":67,\"variation_id\":34,\"name\":\"256GB\"}}','1700.00','','1');
INSERT INTO product_variation_prices VALUES('40','1','{\"33\":{\"id\":66,\"variation_id\":33,\"name\":\"15 Inch\"},\"34\":{\"id\":68,\"variation_id\":34,\"name\":\"512GB\"}}','2000.00','','0');



DROP TABLE IF EXISTS product_variations;

CREATE TABLE `product_variations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_variations_product_id_foreign` (`product_id`),
  CONSTRAINT `product_variations_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO product_variations VALUES('33','1','Screen SIze');
INSERT INTO product_variations VALUES('34','1','Storage');



DROP TABLE IF EXISTS products;

CREATE TABLE `products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `brand_id` bigint(20) unsigned DEFAULT NULL,
  `tax_class_id` bigint(20) unsigned DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(10,2) unsigned NOT NULL,
  `special_price` decimal(10,2) unsigned DEFAULT NULL,
  `special_price_start` date DEFAULT NULL,
  `special_price_end` date DEFAULT NULL,
  `sku` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `manage_stock` tinyint(1) NOT NULL,
  `qty` bigint(20) DEFAULT NULL,
  `in_stock` tinyint(1) NOT NULL,
  `viewed` bigint(20) unsigned NOT NULL DEFAULT 0,
  `is_active` tinyint(1) NOT NULL,
  `featured_tag` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `digital_file` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `products_slug_unique` (`slug`),
  KEY `products_brand_id_foreign` (`brand_id`),
  CONSTRAINT `products_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO products VALUES('1','12','','macbook-pro','variable_product','1200.00','','','','','0','','1','0','1','new','','2020-11-03 14:42:22','2020-11-03 18:29:03');
INSERT INTO products VALUES('2','12','1','iphone-12-128gb','simple_product','799.00','','','','','1','5','1','0','1','new','','2020-11-03 14:47:50','2021-03-06 14:30:29');
INSERT INTO products VALUES('3','','','adobe-photoshop-cs6','digital_product','300.00','279.00','','','','0','','1','0','1','','digital_file/7gIPrlJMhnRiEC31fVoSXxOJwkw6fHXTEd4RNOpr.zip','2020-11-03 19:06:11','2021-03-06 14:26:23');
INSERT INTO products VALUES('4','','','casio-watch','simple_product','15.00','','','','','0','','1','0','1','','','2021-03-12 16:37:49','2021-03-12 16:37:49');



DROP TABLE IF EXISTS roles;

CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS settings;

CREATE TABLE `settings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO settings VALUES('1','backend_direction','ltr','2020-11-05 13:08:02','2021-02-10 19:52:40');
INSERT INTO settings VALUES('2','date_format','d/m/Y','2020-11-05 13:08:02','2021-02-10 19:52:40');
INSERT INTO settings VALUES('3','time_format','24','2020-11-05 13:08:02','2021-02-10 19:52:40');
INSERT INTO settings VALUES('4','allow_singup','yes','2020-11-05 13:08:02','2021-02-10 19:52:40');
INSERT INTO settings VALUES('5','email_verification','enabled','2020-11-05 13:08:02','2021-02-10 19:52:40');
INSERT INTO settings VALUES('6','media_file_types_supported','png,jpg,jpeg','2020-11-05 13:08:02','2021-02-10 19:52:40');
INSERT INTO settings VALUES('7','media_max_upload_size','2','2020-11-05 13:08:02','2021-02-10 19:52:40');
INSERT INTO settings VALUES('8','digital_file_max_upload_size','2','2020-11-05 13:08:02','2021-02-10 19:52:40');
INSERT INTO settings VALUES('9','free_shipping_active','Yes','2020-11-05 13:09:50','2020-11-05 13:57:34');
INSERT INTO settings VALUES('10','free_shipping_label','Free Shipping','2020-11-05 13:09:50','2020-11-05 13:57:34');
INSERT INTO settings VALUES('11','free_shipping_minimum_amount','1000','2020-11-05 13:09:50','2020-11-05 13:57:34');
INSERT INTO settings VALUES('12','company_name','Tricky Code','2020-11-07 13:00:53','2021-03-16 20:13:22');
INSERT INTO settings VALUES('13','site_title','Ultra Store','2020-11-07 13:00:53','2021-03-16 20:13:22');
INSERT INTO settings VALUES('14','phone','+135-545-488','2020-11-07 13:00:53','2020-11-16 17:05:40');
INSERT INTO settings VALUES('15','email','info@trickycode.net','2020-11-07 13:00:53','2020-11-16 17:05:40');
INSERT INTO settings VALUES('16','timezone','Asia/Dhaka','2020-11-07 13:00:53','2020-11-16 17:05:40');
INSERT INTO settings VALUES('17','language','English','2020-11-07 13:00:53','2020-11-16 17:05:40');
INSERT INTO settings VALUES('18','address','Dhaka, Bangladesh','2020-11-07 13:00:54','2020-11-16 17:05:40');
INSERT INTO settings VALUES('19','supported_countries','[\"Bangladesh\",\"United States\"]','2020-11-07 13:08:43','2021-02-10 19:52:40');
INSERT INTO settings VALUES('20','paypal_active','Yes','2020-11-07 13:52:49','2020-11-07 13:54:10');
INSERT INTO settings VALUES('21','paypal_label','PayPal','2020-11-07 13:52:49','2020-11-07 13:54:10');
INSERT INTO settings VALUES('22','paypal_mode','sandbox','2020-11-07 13:52:49','2020-11-07 13:54:10');
INSERT INTO settings VALUES('23','paypal_description','Pay Via PayPal','2020-11-07 13:52:49','2020-11-07 13:54:11');
INSERT INTO settings VALUES('24','paypal_client_id','ASyJ0bT1cHdj1CtwHavl9RF2yKpEhrLUz7XSSz31Kt1o0Unw4TAhjns08sHcA_vrDOV_sA0Y0nmhOzpY','2020-11-07 13:52:49','2020-11-07 13:54:11');
INSERT INTO settings VALUES('25','paypal_secret','ECPVASnvAtPhfqh0S70PPZzh3N5qa13E45W-343zjKyt_7rcePNnQLSdbhWL4oKcdltzJm4YViX5jEwX','2020-11-07 13:52:49','2020-11-07 13:54:11');
INSERT INTO settings VALUES('26','stripe_active','Yes','2020-11-07 13:54:58','2020-11-07 13:55:35');
INSERT INTO settings VALUES('27','stripe_label','Stripe','2020-11-07 13:54:58','2020-11-07 13:55:35');
INSERT INTO settings VALUES('28','stripe_description','Pay Via Credit Card','2020-11-07 13:54:58','2020-11-07 13:55:35');
INSERT INTO settings VALUES('29','stripe_secret_key','sk_test_IJV5nuduGwVdBtihsCIubC5f','2020-11-07 13:54:58','2020-11-07 13:55:35');
INSERT INTO settings VALUES('30','stripe_publishable_key','pk_test_TiUY1pFP2M1rCe6xwlwu9SiS','2020-11-07 13:54:58','2020-11-07 13:55:35');
INSERT INTO settings VALUES('31','razorpay_active','Yes','2020-11-07 13:55:29','2020-11-14 17:42:03');
INSERT INTO settings VALUES('32','razorpay_label','Razorpay','2020-11-07 13:55:29','2020-11-14 17:42:03');
INSERT INTO settings VALUES('33','razorpay_description','Pay Via Razorpay','2020-11-07 13:55:29','2020-11-14 17:42:03');
INSERT INTO settings VALUES('34','razorpay_key_id','rzp_test_lop8725JgyvWPG','2020-11-07 13:55:29','2020-11-14 17:42:03');
INSERT INTO settings VALUES('35','razorpay_key_secret','PiDlAvkK2vXD8igHoVMFHEmt','2020-11-07 13:55:29','2020-11-14 17:42:03');
INSERT INTO settings VALUES('36','paystack_active','Yes','2020-11-07 13:56:02','2020-11-14 18:15:04');
INSERT INTO settings VALUES('37','paystack_label','PayStack','2020-11-07 13:56:02','2020-11-14 18:15:04');
INSERT INTO settings VALUES('38','paystack_description','Pay Via Paystack','2020-11-07 13:56:02','2020-11-14 18:15:04');
INSERT INTO settings VALUES('39','paystack_public_key','pk_test_5fced916578a00175feba5b81f135a877bcc5bd5','2020-11-07 13:56:02','2020-11-14 18:15:04');
INSERT INTO settings VALUES('40','paystack_secret_key','sk_test_3ab6c0f510dd8154b888c0e9860c508767fff5e6','2020-11-07 13:56:02','2020-11-14 18:15:04');
INSERT INTO settings VALUES('41','cod_active','Yes','2020-11-07 13:56:08','2020-11-07 13:56:08');
INSERT INTO settings VALUES('42','cod_label','Cash On Delivery','2020-11-07 13:56:08','2020-11-07 13:56:08');
INSERT INTO settings VALUES('43','cod_description','Cash On Delivery','2020-11-07 13:56:08','2020-11-07 13:56:08');
INSERT INTO settings VALUES('44','bank_transfer_active','Yes','2020-11-07 13:57:35','2020-11-10 20:35:23');
INSERT INTO settings VALUES('45','bank_transfer_label','Bank Transfer','2020-11-07 13:57:36','2020-11-10 20:35:23');
INSERT INTO settings VALUES('46','bank_transfer_description','<p>Make your payment directly into our bank account. You must need to use your Order ID as the payment reference.</p>

<h5>Bank Information</h5>
<b>Bank Name:</b> Test Bank<br>
<b>A/C Name:</b> Jhon Doe<br>
<b>A/C No:</b> 1001654654<br>
<b>Swift Code:</b> 4543552<br>','2020-11-07 13:57:36','2020-11-10 20:35:23');
INSERT INTO settings VALUES('47','bank_transfer_instructions','','2020-11-07 13:57:36','2020-11-10 20:23:21');
INSERT INTO settings VALUES('48','local_pickup_active','Yes','2020-11-07 20:18:03','2020-11-07 20:18:03');
INSERT INTO settings VALUES('49','local_pickup_label','Local Pickup','2020-11-07 20:18:03','2020-11-07 20:18:03');
INSERT INTO settings VALUES('50','local_pickup_cost','20','2020-11-07 20:18:03','2020-11-07 20:18:03');
INSERT INTO settings VALUES('51','flat_rate_active','Yes','2020-11-07 20:18:19','2020-11-07 20:18:19');
INSERT INTO settings VALUES('52','flat_rate_label','Flat Rate','2020-11-07 20:18:19','2020-11-07 20:18:19');
INSERT INTO settings VALUES('53','flat_rate_cost','25','2020-11-07 20:18:19','2020-11-07 20:18:19');
INSERT INTO settings VALUES('54','currency_converter','manual','2020-11-16 17:20:03','2021-01-30 12:49:48');
INSERT INTO settings VALUES('55','fixer_api_key','','2020-11-16 17:20:03','2021-01-30 12:49:48');
INSERT INTO settings VALUES('56','currency_position','left','2020-11-16 17:20:03','2021-01-30 12:49:48');
INSERT INTO settings VALUES('57','thousand_sep',',','2020-11-16 17:20:03','2021-01-30 12:49:48');
INSERT INTO settings VALUES('58','decimal_sep','.','2020-11-16 17:20:03','2021-01-30 12:49:48');
INSERT INTO settings VALUES('59','decimal_places','2','2020-11-16 17:20:03','2021-01-30 12:49:48');
INSERT INTO settings VALUES('60','mail_type','smtp','2020-11-28 14:00:44','2021-02-10 13:27:32');
INSERT INTO settings VALUES('61','from_email','info@trickycode.net','2020-11-28 14:00:44','2021-02-10 13:27:32');
INSERT INTO settings VALUES('62','from_name','TrickyCode','2020-11-28 14:00:44','2021-02-10 13:27:32');
INSERT INTO settings VALUES('63','smtp_host','smtp.mailtrap.io','2020-11-28 14:00:44','2021-02-10 13:27:32');
INSERT INTO settings VALUES('64','smtp_port','2525','2020-11-28 14:00:44','2021-02-10 13:27:32');
INSERT INTO settings VALUES('65','smtp_username','bac58482d29239','2020-11-28 14:00:44','2021-02-10 13:27:32');
INSERT INTO settings VALUES('66','smtp_password','c2039a3ba69228','2020-11-28 14:00:44','2021-02-10 13:27:32');
INSERT INTO settings VALUES('67','smtp_encryption','tls','2020-11-28 14:00:44','2021-02-10 13:27:32');
INSERT INTO settings VALUES('68','primary_menu','1','2021-03-16 22:45:35','2021-03-17 16:03:43');
INSERT INTO settings VALUES('69','category_menu','2','2021-03-16 22:45:35','2021-03-17 16:03:43');
INSERT INTO settings VALUES('70','footer_menu_1_title','','2021-03-16 22:45:35','2021-03-17 16:03:43');
INSERT INTO settings VALUES('71','footer_menu_1','','2021-03-16 22:45:35','2021-03-17 16:03:43');
INSERT INTO settings VALUES('72','footer_menu_2_title','','2021-03-16 22:45:35','2021-03-17 16:03:43');
INSERT INTO settings VALUES('73','footer_menu_2','','2021-03-16 22:45:35','2021-03-17 16:03:43');
INSERT INTO settings VALUES('74','footer_about_us','','2021-03-16 22:49:23','2021-03-16 22:49:23');
INSERT INTO settings VALUES('75','copyright_text','','2021-03-16 22:49:23','2021-03-16 22:49:23');
INSERT INTO settings VALUES('76','payment_method_image','file_1615913363.png','2021-03-16 22:49:23','2021-03-16 22:49:23');



DROP TABLE IF EXISTS tags;

CREATE TABLE `tags` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tags_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO tags VALUES('1','electronics','2020-11-02 22:34:02','2020-11-02 22:34:02');
INSERT INTO tags VALUES('2','laptop','2020-11-02 22:34:19','2020-11-02 22:34:19');
INSERT INTO tags VALUES('3','desktop','2020-11-02 22:34:23','2020-11-02 22:34:23');
INSERT INTO tags VALUES('4','camera','2020-11-02 22:34:30','2020-11-02 22:34:30');
INSERT INTO tags VALUES('5','mouse','2020-11-02 22:34:34','2020-11-02 22:34:34');
INSERT INTO tags VALUES('6','shoes','2020-11-02 22:34:38','2020-11-02 22:34:38');
INSERT INTO tags VALUES('7','bags','2020-11-02 22:34:44','2020-11-02 22:34:44');
INSERT INTO tags VALUES('8','jeans','2020-11-02 22:34:49','2020-11-02 22:34:49');
INSERT INTO tags VALUES('9','keyboard','2020-11-02 22:35:03','2020-11-02 22:35:03');
INSERT INTO tags VALUES('10','ssd','2020-11-02 22:35:09','2020-11-02 22:35:09');



DROP TABLE IF EXISTS tags_translation;

CREATE TABLE `tags_translation` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tag_id` bigint(20) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tags_translation_tag_id_locale_unique` (`tag_id`,`locale`),
  CONSTRAINT `tags_translation_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO tags_translation VALUES('1','1','English','Electronics','2020-11-02 22:34:02','2020-11-02 22:34:02');
INSERT INTO tags_translation VALUES('2','2','English','Laptop','2020-11-02 22:34:19','2020-11-02 22:34:19');
INSERT INTO tags_translation VALUES('3','3','English','Desktop','2020-11-02 22:34:23','2020-11-02 22:34:23');
INSERT INTO tags_translation VALUES('4','4','English','Camera','2020-11-02 22:34:30','2020-11-02 22:34:30');
INSERT INTO tags_translation VALUES('5','5','English','Mouse','2020-11-02 22:34:34','2020-11-02 22:34:34');
INSERT INTO tags_translation VALUES('6','6','English','Shoes','2020-11-02 22:34:38','2020-11-02 22:34:38');
INSERT INTO tags_translation VALUES('7','7','English','Bags','2020-11-02 22:34:44','2020-11-02 22:34:44');
INSERT INTO tags_translation VALUES('8','8','English','Jeans','2020-11-02 22:34:49','2020-11-02 22:34:49');
INSERT INTO tags_translation VALUES('9','9','English','Keyboard','2020-11-02 22:35:03','2020-11-02 22:35:03');
INSERT INTO tags_translation VALUES('10','10','English','SSD','2020-11-02 22:35:09','2020-11-02 22:35:09');



DROP TABLE IF EXISTS tax_classes;

CREATE TABLE `tax_classes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `based_on` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO tax_classes VALUES('1','shipping_address','2020-11-07 13:09:38','2020-11-07 13:09:38');



DROP TABLE IF EXISTS tax_classes_translation;

CREATE TABLE `tax_classes_translation` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tax_class_id` bigint(20) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tax_classes_translation_tax_class_id_locale_unique` (`tax_class_id`,`locale`),
  CONSTRAINT `tax_classes_translation_tax_class_id_foreign` FOREIGN KEY (`tax_class_id`) REFERENCES `tax_classes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO tax_classes_translation VALUES('1','1','English','General Tax','2020-11-07 13:09:38','2020-11-07 13:09:38');



DROP TABLE IF EXISTS tax_rates;

CREATE TABLE `tax_rates` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tax_class_id` bigint(20) unsigned NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` decimal(8,4) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tax_rates_tax_class_id_index` (`tax_class_id`),
  CONSTRAINT `tax_rates_tax_class_id_foreign` FOREIGN KEY (`tax_class_id`) REFERENCES `tax_classes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO tax_rates VALUES('1','1','BD','Dhaka','10.0000','2020-11-07 13:09:38','2020-11-11 14:06:20');
INSERT INTO tax_rates VALUES('2','1','BD','Dhaka','5.0000','2020-11-07 13:09:38','2020-11-16 17:19:02');



DROP TABLE IF EXISTS tax_rates_translation;

CREATE TABLE `tax_rates_translation` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tax_rate_id` bigint(20) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tax_rates_translation_tax_rate_id_locale_unique` (`tax_rate_id`,`locale`),
  CONSTRAINT `tax_rates_translation_tax_rate_id_foreign` FOREIGN KEY (`tax_rate_id`) REFERENCES `tax_rates` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO tax_rates_translation VALUES('1','1','English','Tax Rate 1','2020-11-07 13:09:38','2020-11-07 13:09:38');
INSERT INTO tax_rates_translation VALUES('2','2','English','Tax Rate 2','2020-11-07 13:09:38','2020-11-07 13:09:38');



DROP TABLE IF EXISTS transactions;

CREATE TABLE `transactions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) unsigned NOT NULL,
  `transaction_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_method` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `payment_data` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transactions_order_id_foreign` (`order_id`),
  CONSTRAINT `transactions_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO transactions VALUES('1','1','84C350252W0122421','PayPal','279.50','','2020-11-14 21:48:19','2020-11-14 21:48:19');
INSERT INTO transactions VALUES('2','16','6S433983PT046094W','PayPal','1200.00','','2021-02-11 13:04:17','2021-02-11 13:04:17');
INSERT INTO transactions VALUES('4','15','112124','Cash_On_Delivery','943.85','','2021-02-11 13:53:06','2021-02-11 13:53:06');



DROP TABLE IF EXISTS users;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `profile_picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO users VALUES('1','Jhon Doe','admin@demo.com','+674588448','admin','','1','default.png','2021-02-06 20:07:46','$2y$10$uPjAEeHWzudR3VJmqhlZJOJ5b29/jkrKTmZ2NZ/iQePQrUOcsyRDi','','','','2020-11-01 17:55:21','2021-01-13 14:06:53');
INSERT INTO users VALUES('3','Demo Customer','customer@demo.com','16584564','customer','','1','default.png','2020-11-08 19:07:27','$2y$10$vzSr3YqVAlHXYfoaCEemveqvXe2XLTBOzSoM7ozEtCWvTntJENAeC','','','','2020-11-08 19:07:27','2020-11-08 21:18:13');
INSERT INTO users VALUES('18','Kuddus','kuddus@gmail.com','135485154','customer','','1','default.png','2020-11-13 14:05:54','$2y$10$y7ts9qO1Z99qQUOKJO72TO0EWFLw08dF1AOZIzMiRPc6oq8neDVqu','','','ejHiFVdxBxEmXMp8lZNpZPfwYZPdhAlSvD5s60BHp8xrMfe6UU0rZxMi542K','2020-11-13 14:05:54','2020-11-13 14:05:54');
INSERT INTO users VALUES('38','Kennedy','kennedyrodrick93@gmail.com','01825644814','customer','','1','default.png','2021-02-10 14:43:13','$2y$10$O7FtuDuLmspUFofrYxm56OAQALWIDOiipagkEeVBXBbMh414VpWS.','','','','2021-02-10 13:27:50','2021-02-10 14:43:13');
INSERT INTO users VALUES('39','TrickyCode','trickycode93@gmail.com','01825567058','customer','','1','default.png','2021-03-06 07:23:50','$2y$10$31ouGA82SHtrqRCR7/yg5eFwU8YyVms7JOYGZe/qNbpGxMKIpuENK','','','','2021-03-06 13:22:18','2021-03-06 07:23:50');



DROP TABLE IF EXISTS wish_lists;

CREATE TABLE `wish_lists` (
  `user_id` bigint(20) unsigned NOT NULL,
  `product_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`,`product_id`),
  KEY `wish_lists_product_id_foreign` (`product_id`),
  CONSTRAINT `wish_lists_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  CONSTRAINT `wish_lists_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO wish_lists VALUES('1','1','2021-02-11 14:21:31','2021-02-11 14:21:31');
INSERT INTO wish_lists VALUES('1','2','2021-02-11 14:21:27','2021-02-11 14:21:27');
INSERT INTO wish_lists VALUES('1','3','2021-02-11 14:20:55','2021-02-11 14:20:55');
INSERT INTO wish_lists VALUES('39','3','2021-03-06 13:24:18','2021-03-06 13:24:18');



