@extends('theme.default.website')

@section('content')

    <link rel="stylesheet" href="{{ asset('public/theme/default/css/css-stars.css') }}">

    <!-- Breadcrumbs -->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="bread-inner">
                        <ul class="bread-list">
                            <li><a href="{{ url('') }}">{{ _lang('Home') }}<i class="ti-arrow-right"></i></a></li>
                            @foreach($product->categories as $category)
                                <li>
                                    <a href="{{ url('/categories/'.$category->slug) }}">{{ $category->translation->name }}
                                        <i
                                                class="ti-arrow-right"></i></a>
                                </li>
                            @endforeach

                            <li class="active">
                                <a href="{{ url('/shop/'.$product->slug) }}">{{ $product->translation->name==""?$product->default_lang[0]->name:$product->translation->name }}</a>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Breadcrumbs -->


    <section class="shop single section">
        <div class="container">
            <div class="row mb-4">
                <div class="col-12">
                    @if(isset($product->gallery_images))
                        <div class="row">
                            <div class="col-lg-7 col-12">
                                <!-- Product Slider -->
                                <div class="product-gallery">
                                    <!-- Images slider -->
                                    <div class="row">
                                        <div class="col-3 text-center">
                                            {{--                                    <a class="pro-gallery-up" href="#" data-slide="prev">--}}
                                            {{--                                        <i class="fa fa-chevron-up"></i>--}}
                                            {{--                                    </a>--}}
                                            <div onclick="changeBigImage('{{gcpUrl($product->image->file_path)}}', this)"
                                                 class="small-image-slide mb-3 active_item">
                                                <img src="{{gcpUrl($product->image->file_path,'256x256')}}" alt="">
                                            </div>
                                            @foreach($product->gallery_images as $gallery_image)
                                                <div onclick="changeBigImage('{{gcpUrl($gallery_image->file_path)}}', this)"
                                                     class="small-image-slide {{ $loop->index==2?"":"mb-3" }} {{ $loop->index>=3?"hide":"" }}">
                                                    <img src="{{gcpUrl($gallery_image->file_path,'256x256')}}"
                                                         alt="">
                                                </div>
                                            @endforeach



                                            {{--                                    <a class="pro-gallery-down" href="#" data-slide="next">--}}
                                            {{--                                        <i class="fa fa-chevron-down"></i>--}}
                                            {{--                                    </a>--}}
                                        </div>
                                        <div class="col-9 pl-0">
                                            <div class="">
                                                <img src="{{gcpUrl($product->image->file_path)}}" id="big-image"
                                                     alt="">
                                            </div>
                                        </div>

                                    </div>
                                    <!-- <div class="container-small-image">

                                        </div> -->

                                    <!-- End Images slider -->

                                </div>
                                <!-- End Product slider -->
                            </div>
                            <div class="col-lg-5 col-12">
                                <div class="product-des">
                                    <!-- Description -->
                                    <div class="short">
                                        <h4>{{ $product->translation->name==""?$product->default_lang[0]->name:$product->translation->name  }}</h4>
                                        @if($product->product_type != 'variable_product')
                                            <p class="price">
                                                @if($product->special_price != '' || (int) $product->special_price != 0 )
                                                    <span class="discount">{!! xss_clean(show_price($product->special_price)) !!}</span>
                                                    <s>{!! xss_clean(show_price($product->price)) !!}</s>
                                                @else
                                                    <span class="discount">{!! xss_clean(show_price($product->price)) !!}</span>
                                                @endif
                                            </p>
                                        @else
                                            <p class="price">
                                                @if($product->variation_prices[0]->special_price != '' || (int)
                                                $product->variation_prices[0]->special_price != 0 )
                                                    <span class="discount">{!!
                                        xss_clean(show_price($product->variation_prices[0]->special_price)) !!}</span>
                                                    <s>{!! xss_clean(show_price($product->variation_prices[0]->price)) !!}</s>
                                                @else
                                                    <span class="discount">{!!
                                        xss_clean(show_price($product->variation_prices[0]->price)) !!}</span>
                                                @endif
                                            </p>
                                        @endif

                                        <p class="description">{{ $product->translation->short_description==""?$product->default_lang[0]->short_description:$product->translation->short_description }}</p>
                                    </div>
                                    <!--/ End Description -->


                                    <!-- Product Options -->
                                    @if(! $product->product_options->isEmpty())
                                        <form action="{{ url('products/get_variation_price/'.$product->id) }}"
                                              id="product-variation">
                                            @csrf
                                            @foreach($product->product_options as $product_option)
                                                <div class="product_options">
                                                    <h6>{{ $product_option->name }}</h6>
                                                    <select name="product_option[]" class="select_product_option">
                                                        @foreach($product_option->items as $item)
                                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            @endforeach
                                        </form>
                                        <div class="clearfix"></div>
                                @endif

                                <!--/ End Product Options -->

                                    <!-- Product Buy -->
                                    <div class="product-buy">

                                        <!-- Description Tab -->
                                        <div class="tab-pane fade show active" id="description" role="tabpanel">
                                            <div class="tab-single">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="single-des mt-3">
                                                            {!! xss_clean($product->translation->description==""?$product->default_lang[0]->description:$product->translation->description) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/ End Description Tab -->
                                        <p class="cat">{{ _lang('Categories') }} :
                                            {{ $product->categories->count() == 0 ? _lang('N/A') : '' }}
                                            @foreach($product->categories as $category)
                                                <a
                                                        href="{{ url('/categories/'.$category->slug) }}">{{ $category->translation->name }}</a>
                                            @endforeach
                                        </p>

                                        @if($product->sku != '')
                                            <p class="sku">{{ _lang('SKU') }} : {{ $product->sku }}</p>
                                        @endif

                                        @if($product->manage_stock == 1 && $product->in_stock == 1)
                                            <p class="availability">{{ _lang('Availability') }} :
                                                {{ $product->qty.' '._lang('Product In Stock') }}</p>
                                        @endif

                                        @if($product->in_stock == 0)
                                            <p class="availability">{{ _lang('Availability') }} : <span
                                                        class="text-danger">{{ _lang('Out Of Stock') }}</span></p>
                                        @endif

                                    </div>
                                    <!--/ End Product Buy -->

                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>

            @if($product->video_url)
                <div class="video-section">
                    <ul class="transform-bg-style" role="tablist">
                        <li class="title font-strong d-flex justify-content-between">
                            <a href="#">{{ _lang('Video') }}
                                <div class="corner"></div>
                            </a>
                            <div class="see-more-video">
                                <a href="https://www.youtube.com/channel/UCzhZ7qCeP_OJ9BM3HHVD0IA"
                                   target="_blank">{{ _lang('See more video') }}</a>
                                <i class="fa fa-angle-right"></i>
                            </div>
                        </li>
                    </ul>
                    <div class="mt-3">
                        <div class="iframe-container">
                            <iframe src="https://www.youtube.com/embed/{{$product->video_url}}"
                                    title="YouTube video player"
                                    frameborder="0"
                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            @endif

            @if(count($product->detail_images)>0)
                <div class="image-detail-section">
                    @include('theme.default.components.category-title',['category_name' => _lang('Detail about product') ,'link' => '#'])
                    <div class="mt-3">
                        <div class="product-detail-image">
                            @foreach($product->detail_images as $item)
                                <div class="mb-3">
                                    <img src="{{gcpUrl($item->file_path,'256x256')}}" alt="">
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
            @if(count($product_relate))
                <div class="row">
                    <div class="col-12">
                        <div class="product-info">
                        <!-- <div class="nav-main">
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#description" role="tab">{{ _lang('Description') }}</a></li>
                                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#reviews" role="tab">{{ _lang('Reviews') }} ({{ $product->reviews->count() }})</a></li>
                                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#comments" role="tab">{{ _lang('Comments') }} ({{ $product->comments->count() }})</a></li>
                                    </ul>
                                </div> -->
                            <div class="tab-content" id="myTabContent">
                                <!-- Recommend product -->
                                <div class="row">
                                    <div class="col-12">
                                        @include('theme.default.components.category-title',['category_name' => _lang('Recommend for you'),'link' => '#'])
                                        <div class="recommend-slider">
                                            <section class="pt- circle-1">
                                                <div class="container position-relative">
                                                    <div id="product_relate" class="carousel slide position-relative"
                                                         data-ride="carousel" data-interval="8000">
                                                        <!-- The slideshow -->
                                                        <div class="carousel-inner mt-3">
                                                            @foreach($product_relate->chunk(4) as $relate)
                                                                <div class="carousel-item {{$loop->iteration==1 ? 'active' : ''}} ">
                                                                    <div class="d-flex">
                                                                        @foreach($relate as $product)
                                                                            <div class="col-lg-4 col-md-6 col-12 col-xl-3 {{ isset($class) ? $class : '' }}">
                                                                                <div class="single-product">
                                                                                    <div class="product-img">
                                                                                        <a href="{{ url('product/'.$product->slug) }}">
                                                                                            <img class="default-img"
                                                                                                 src="{{ gcpUrl($product->image->file_path,'256x256') }}"
                                                                                                 alt="{{ $product->translation->name }}">
                                                                                            @if($product->in_stock == 0)
                                                                                                <span class="out-of-stock">{{ _lang('Out Of Stock') }}</span>
                                                                                            @elseif($product->featured_tag != NULL)
                                                                                                <span class="{{ $product->featured_tag }}">{{ _dlang(str_replace('_',' ',$product->featured_tag)) }}</span>
                                                                                            @endif
                                                                                        </a>
                                                                                        <div class="button-head">
                                                                                            <div class="product-action">
                                                                                                <a href="{{ url('product/'.$product->slug) }}"
                                                                                                   title="{{ _lang('Quick View') }}"
                                                                                                   class="quick-shop">
                                                                                                    <i class=" ti-eye"></i><span>{{ _lang('Quick Shop') }}</span>
                                                                                                </a>

                                                                                                {{--						<a title="{{ _lang('Wishlist') }}" class="btn-wishlist" href="{{ wishlist_url($product) }}"><i class=" ti-heart "></i><span>{{ _lang('Add to Wishlist') }}</span></a>--}}
                                                                                            </div>
                                                                                            <div class="product-action-2">
                                                                                                @if($product->product_type != 'variable_product')
                                                                                                    @if($product->in_stock == 1)
                                                                                                        <a title="View product detail"
                                                                                                           class="add_to_cart"
                                                                                                           data-type="{{ $product->product_type }}"
                                                                                                           href="{{ url('product/'.$product->slug) }}">{{ _lang('View product detail') }}</a>
                                                                                                    @else
                                                                                                        <a title="View product detail"
                                                                                                           class="btn-link disabled"
                                                                                                           href="{{ url('product/'.$product->slug) }}">{{ _lang('View product detail') }}</a>
                                                                                                    @endif
                                                                                                @else
                                                                                                    <a title="View product detail"
                                                                                                       href="{{ url('product/'.$product->slug) }}">{{ _lang('View product detail') }}</a>
                                                                                                @endif
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="product-content">

                                                                                        <h3>
                                                                                            <a href="{{ url('product/'.$product->slug) }}">{{ $product->translation->name==""?$product->default_lang[0]->name:$product->translation->name }}</a>
                                                                                        </h3>

                                                                                        @if($product->product_type != 'variable_product')
                                                                                            <div class="product-price">
                                                                                                @if($product->special_price != '' || (int) $product->special_price != 0 )
                                                                                                    <span class="text-danger">
                                                                                                        <s>{!! xss_clean(show_price($product->price)) !!}</s>
                                                                                                    </span>
                                                                                                    <span class="text-success">{!! xss_clean(show_price($product->special_price)) !!}</span>
                                                                                                @else
                                                                                                    <span>{!! xss_clean(show_price($product->price)) !!}</span>
                                                                                                @endif
                                                                                            </div>
                                                                                        @else
                                                                                            <div class="product-price">
                                                                                                @if($product->variation_prices[0]->special_price != '' || (int) $product->variation_prices[0]->special_price != 0 )
                                                                                                    <span class="text-danger">
                                                                                                        <s>{!! xss_clean(show_price($product->variation_prices[0]->price)) !!}</s>
                                                                                                    </span>
                                                                                                    <span class="text-success">
                                                                                                        {!! xss_clean(show_price($product->variation_prices[0]->special_price)) !!}
                                                                                                        -
                                                                                                        {!! xss_clean(show_price($product->variation_prices[count($product->variation_prices) - 1]->special_price)) !!}
                                                                                                    </span>
                                                                                                @else
                                                                                                    <span>
                                                                                                        {!! xss_clean(show_price($product->variation_prices[0]->price)) !!}
                                                                                                        -
                                                                                                        {!! xss_clean(show_price($product->variation_prices[count($product->variation_prices) - 1]->price)) !!}
                                                                                                    </span>
                                                                                                @endif

                                                                                            </div>
                                                                                        @endif
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        @endforeach
                                                                    </div>

                                                                </div>
                                                            @endforeach
                                                        </div>

                                                        <!-- Left and right controls -->
                                                        <a class="carousel-control-prev" href="#product_relate"
                                                           data-slide="prev">
                                                            <i class="fa fa-chevron-left"></i>
                                                        </a>
                                                        <a class="carousel-control-next" href="#product_relate"
                                                           data-slide="next">
                                                            <i class="fa fa-chevron-right"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            @if(count($latest_relate))
                <div class="row">
                    <div class="col-12">
                        <div class="product-info">
                        <!-- <div class="nav-main">
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#description" role="tab">{{ _lang('Description') }}</a></li>
                                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#reviews" role="tab">{{ _lang('Reviews') }} ({{ $product->reviews->count() }})</a></li>
                                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#comments" role="tab">{{ _lang('Comments') }} ({{ $product->comments->count() }})</a></li>
                                    </ul>
                                </div> -->
                            <div class="tab-content" id="myTabContent">
                                <!-- Recommend product -->
                                <div class="row">
                                    <div class="col-12">
                                        @include('theme.default.components.category-title',['category_name' => _lang('Latest product'),'link' => '#'])
                                        <div class="recommend-slider">
                                            <section class="pt- circle-1">
                                                <div class="container position-relative">
                                                    <div id="latest_relate" class="carousel slide position-relative"
                                                         data-ride="carousel" data-interval="8000">

                                                        <!-- The slideshow -->
                                                        <div class="carousel-inner mt-3">
                                                            @foreach($latest_relate->chunk(4) as $relate)
                                                                <div class="carousel-item {{$loop->iteration==1 ? 'active' : ''}} ">
                                                                    <div class="d-flex">
                                                                        @foreach($relate as $product)
                                                                            <div class="col-lg-4 col-md-6 col-12 col-xl-3 {{ isset($class) ? $class : '' }}">
                                                                                <div class="single-product">
                                                                                    <div class="product-img">
                                                                                        <a href="{{ url('product/'.$product->slug) }}">
                                                                                            <img class="default-img"
                                                                                                 src="{{ gcpUrl($product->image->file_path,'256x256') }}"
                                                                                                 alt="{{ $product->translation->name }}">
                                                                                            @if($product->in_stock == 0)
                                                                                                <span class="out-of-stock">{{ _lang('Out Of Stock') }}</span>
                                                                                            @elseif($product->featured_tag != NULL)
                                                                                                <span class="{{ $product->featured_tag }}">{{ _dlang(str_replace('_',' ',$product->featured_tag)) }}</span>
                                                                                            @endif
                                                                                        </a>
                                                                                        <div class="button-head">
                                                                                            <div class="product-action">
                                                                                                <a href="{{ url('product/'.$product->slug) }}"
                                                                                                   title="{{ _lang('Quick View') }}"
                                                                                                   class="quick-shop">
                                                                                                    <i class=" ti-eye"></i><span>{{ _lang('Quick Shop') }}</span>
                                                                                                </a>

                                                                                                {{--						<a title="{{ _lang('Wishlist') }}" class="btn-wishlist" href="{{ wishlist_url($product) }}"><i class=" ti-heart "></i><span>{{ _lang('Add to Wishlist') }}</span></a>--}}
                                                                                            </div>
                                                                                            <div class="product-action-2">
                                                                                                @if($product->product_type != 'variable_product')
                                                                                                    @if($product->in_stock == 1)
                                                                                                        <a title="View product detail"
                                                                                                           class="add_to_cart"
                                                                                                           data-type="{{ $product->product_type }}"
                                                                                                           href="{{ url('product/'.$product->slug) }}">{{ _lang('View product detail') }}</a>
                                                                                                    @else
                                                                                                        <a title="View product detail"
                                                                                                           class="btn-link disabled"
                                                                                                           href="{{ url('product/'.$product->slug) }}">{{ _lang('View product detail') }}</a>
                                                                                                    @endif
                                                                                                @else
                                                                                                    <a title="View product detail"
                                                                                                       href="{{ url('product/'.$product->slug) }}">{{ _lang('View product detail') }}</a>
                                                                                                @endif
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="product-content">

                                                                                        <h3>
                                                                                            <a href="{{ url('product/'.$product->slug) }}">{{ $product->translation->name==""?$product->default_lang[0]->name:$product->translation->name }}</a>
                                                                                        </h3>

                                                                                        @if($product->product_type != 'variable_product')
                                                                                            <div class="product-price">
                                                                                                @if($product->special_price != '' || (int) $product->special_price != 0 )
                                                                                                    <span class="text-danger">
                                                                                        <s>{!! xss_clean(show_price($product->price)) !!}</s>
                                                                                    </span>
                                                                                                    <span class="text-success">{!! xss_clean(show_price($product->special_price)) !!}</span>
                                                                                                @else
                                                                                                    <span>{!! xss_clean(show_price($product->price)) !!}</span>
                                                                                                @endif
                                                                                            </div>
                                                                                        @else
                                                                                            <div class="product-price">
                                                                                                @if($product->variation_prices[0]->special_price != '' || (int) $product->variation_prices[0]->special_price != 0 )
                                                                                                    <span class="text-danger">
                                                                                        <s>{!! xss_clean(show_price($product->variation_prices[0]->price)) !!}</s>
                                                                                    </span>
                                                                                                    <span class="text-success">
                                                                                        {!! xss_clean(show_price($product->variation_prices[0]->special_price)) !!}
                                                                                        -
                                                                                        {!! xss_clean(show_price($product->variation_prices[count($product->variation_prices) - 1]->special_price)) !!}
                                                                                    </span>
                                                                                                @else
                                                                                                    <span>
                                                                                        {!! xss_clean(show_price($product->variation_prices[0]->price)) !!}
                                                                                        -
                                                                                        {!! xss_clean(show_price($product->variation_prices[count($product->variation_prices) - 1]->price)) !!}
                                                                                    </span>
                                                                                                @endif

                                                                                            </div>
                                                                                        @endif


                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        @endforeach
                                                                    </div>

                                                                </div>
                                                            @endforeach
                                                        </div>

                                                        <!-- Left and right controls -->

                                                        <a class="carousel-control-prev" href="#latest_relate"
                                                           data-slide="prev">
                                                            <i class="fa fa-chevron-left"></i>
                                                        </a>
                                                        <a class="carousel-control-next" href="#latest_relate"
                                                           data-slide="next">
                                                            <i class="fa fa-chevron-right"></i>
                                                        </a>
                                                    </div>


                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </section>

@endsection

@section('js-script')
    <script src="{{ asset('public/theme/default/js/jquery.barrating.min.js') }}"></script>
    <script src="{{ asset('public/theme/default/js/product-options.js') }}"></script>
    <script src="{{ asset('public/theme/default/js/cart.js') }}"></script>

    <script>
        function changeBigImage(imageUrl, currentSlide) {
            let bigImage = document.querySelector('img#big-image');
            let smallImageSlide = document.querySelectorAll('.small-image-slide, .active_item');
            console.log({smallImageSlide});
            bigImage.src = imageUrl;
            if (smallImageSlide) {
                smallImageSlide.forEach(item => {
                    item.classList.remove('active_item');
                });
            }
            currentSlide.classList.add('active_item');
        }
    </script>



@endsection