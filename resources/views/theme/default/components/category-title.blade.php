<ul class="transform-bg-style" role="tablist">
    <li class="title font-strong d-flex justify-content-between">
        <a href="{{$link}}">{{$category_name}}
{{--            <i class="fa fa-angle-right"></i>--}}
            <div class="corner"></div>
        </a>
        @if($link != '#')
            <div class="see-more-video">

                <a href="{{$link}}">{{ _lang('See more') }}</a>
                <i class="fa fa-angle-right"></i>
            </div>
        @endif
    </li>
</ul>