<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Meta Tag -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name='copyright' content=''>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title Tag  -->
    <title>{{ isset($seo_title) ? $seo_title : get_option('site_title', config('app.name')) }}</title>

    <meta name="keywords" content="{{ isset($meta_keywords) ? $meta_keywords : get_option('meta_keywords') }}" />
    <meta name="description"
        content="{{ isset($meta_description) ? $meta_description : get_option('meta_description') }}" />

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="{{ get_favicon() }}">

    <!-- Web Font -->
    <link
        href="https://fonts.googleapis.com/css?family=Poppins:200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap"
        rel="stylesheet">

    <!-- StyleSheet -->

    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ asset('public/theme/default/css/bootstrap.css') }}">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="{{ asset('public/theme/default/css/magnific-popup.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('public/theme/default/css/font-awesome.css') }}">
    <!-- Fancybox -->
    <link rel="stylesheet" href="{{ asset('public/theme/default/css/jquery.fancybox.min.css') }}">
    <!-- Themify Icons -->
    <link rel="stylesheet" href="{{ asset('public/theme/default/css/themify-icons.css') }}">
    <!-- Nice Select CSS -->
    <link rel="stylesheet" href="{{ asset('public/theme/default/css/niceselect.css') }}">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{ asset('public/theme/default/css/animate.css') }}">
    <!-- Flex Slider CSS -->
    <link rel="stylesheet" href="{{ asset('public/theme/default/css/flex-slider.min.css') }}">
    <!-- Jquery Ui -->
    <link rel="stylesheet" href="{{ asset('public/theme/default/css/jquery-ui.css') }}">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="{{ asset('public/theme/default/css/owl-carousel.css') }}">
    <!-- Slicknav -->
    <link rel="stylesheet" href="{{ asset('public/theme/default/css/slicknav.min.css') }}">

    <link href="{{ asset('public/backend/plugins/jquery-toast-plugin/jquery.toast.min.css') }}" rel="stylesheet" />

    <link rel="stylesheet" href="{{ asset('public/theme/default/css/reset.css') }}">
    <link rel="stylesheet" href="{{ asset('public/theme/default/style.css') }}">
    <link rel="stylesheet" href="{{ asset('public/theme/default/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('public/theme/default/css/responsive.css') }}">


    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link
        href="https://fonts.googleapis.com/css2?family=Hanuman:wght@400;700&family=Roboto:ital,wght@0,100;0,300;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap"
        rel="stylesheet">

    <style>
    @import url('https://fonts.googleapis.com/css2?family=Koulen&display=swap');

    @font-face {
        font-family: "Siemreap";
        src: url('{{ asset('public/theme/default/fonts/Siemreap-Regular.ttf') }}'),
    }

    @font-face {
        font-family: "Koulen";
        src: url('{{ asset('public/theme/default/fonts/Koulen-Regular.ttf') }}'),
    }

    @font-face {
        font-family: "Montserrat-Bold";
        src: url('{{ asset('public/theme/default/fonts/Montserrat-Bold.ttf') }}'),
    }

    @font-face {
        font-family: "Montserrat-ExtraBold";
        src: url('{{ asset('public/theme/default/fonts/Montserrat-ExtraBold.ttf') }}'),
    }

    p,
    i,
    b,
    h1,
    h2,
    h3,
    h4,
    h5,
    a,
    span,
    li {
        font-family: 'Poppins', 'Hanuman';
    }
    </style>

    @include('theme.default.components.custom_styles')

    <script type="text/javascript">
        var _url = "{{ url('') }}";
    </script>
    <script>
    function openNav() {
        document.getElementById("mySidepanel").style.height = "556px";
        // document.getElementById("mySidepanel").style.minHeight = "0px";
        document.getElementById("mySidepanel").style.width = "300px";
    }
    function closeNav() {
        document.getElementById("mySidepanel").style.height = "0";
    }
    </script>


</head>

<body class="js">
    <!-- Preloader -->
{{--    <div class="preloader">--}}
{{--        <div class="preloader-inner">--}}
{{--            <div class="preloader-icon">--}}
{{--                <span></span>--}}
{{--                <span></span>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
    <!-- End Preloader -->

    <!-- Header -->
    <header class="header shop">
        <!-- Topbar -->
        <div class="topbar">
            <div class="container">
                <div class="row justify-content-between px-1">
                    <!-- Top Left -->
                    <div class="top-left">
                        <ul class="list-main">
                            <li><i class="fa fa-phone"></i> {{ get_option('phone') }}</li>
                            <li><i class="ti-email"></i> {{ get_option('email') }}</li>
                        </ul>
                    </div>
                    <!--/ End Top Left -->
{{--                    <div class="right-content">--}}
{{--                        <div class="list-main">--}}
{{--                            <div class="dropdown show">--}}
{{--                                <a class="dropdown-toggle" href="#" role="button" id="languageSwitcher"--}}
{{--                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
{{--                                    <i class="ti-world"></i>--}}
{{--                                    {{ session('language') =='' ? get_option('language') : session('language') }}--}}
{{--                                </a>--}}
{{--                                <div class="dropdown-menu" aria-labelledby="languageSwitcher">--}}
{{--                                    @foreach(get_language_list() as $language)--}}
{{--                                    <a class="dropdown-item"--}}
{{--                                        href="{{ url('/') }}?language={{ $language }}">{{ $language }}</a>--}}
{{--                                    @endforeach--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
        <!-- End Topbar -->
        <div class="middle-inner">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-12">
                        <div class="row">
                            <div class="col-9">
                                <div class="logo">
                                    <a href="{{ url('') }}"><img src="{{ get_logo() }}" alt="logo"></a>
                                </div>
                            </div>
                            <div class="col-3 my-auto">
                                <div class="search-top">
                                    <div class="top-search"><a href="#0"><i class="ti-search"></i></a></div>
                                    <!-- Search Form -->
                                    <div class="search-top">
                                        <form class="search-form" action="{{ url('/shop') }}">
                                            <input type="text" class="search-products"
                                                placeholder="{{ _lang('Search here') }}..." name="search">
                                            <button value="search" type="submit"><i class="ti-search"></i></button>
                                        </form>
                                    </div>
                                    <!--/ End Search Form -->
                                </div>
                                <div class="mobile-nav"></div>
                            </div>
                        </div>
                        <!--/ End Search Form -->
                    </div>
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="search-bar-top">
                            <form action="{{ url('/shop') }}">
                                <div class="search-bar">
                                    @php
                                        $search_category = isset($_GET['category']) ? $_GET['category'] : '';
                                        $categories = App\Entity\Category\Category::where('parent_id',null)->with(["default_lang","translation"])->get();

                                    @endphp
                                    <select class="nice-select" name="category" id="search-category">
                                        <option value="all">{{ _lang('All') }}</option>
                                        @foreach($categories as $category)
                                        <option value="{{ $category->slug }}"
                                            {{ $search_category == $category->slug ? 'selected' : ''  }}>
                                            {{ $category->translation->name==""?$category->default_lang[0]->name:$category->translation->name }}</option>
                                        @endforeach
                                    </select>

                                    <input name="search" class="search-products"
                                        placeholder="{{ _lang('Search Products') }}" type="search">
                                    <button class="btnn"><i class="ti-search"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-12">
                        <div class="right-bar">

                            <div class="sinlge-bar">
                                <a target="_blank" href="{{ get_option('facebook_link') }}" class="single-icon">
                                    <i class="fa fa-facebook-square" aria-hidden="true"></i>
                                </a>
                            </div>
                            <div class="sinlge-bar">
                                <a target="_blank" href="{{ get_option('instagram_link') }}" class="single-icon">
                                    <i class="fa fa-instagram" aria-hidden="true"></i>
                                </a>
                            </div>
                            <div class="sinlge-bar">
                                <a target="_blank" href="{{ get_option('youtube_link') }}" class="single-icon">
                                    <i class="fa fa-youtube" aria-hidden="true"></i>
                                </a>
                            </div>
                            <div class="sinlge-bar">
                                <a target="_blank" href="{{ get_option('twitter_link') }}" class="single-icon">
                                    <img src="{{ asset('public/theme/default/images/tamneak_icon_black.png') }}"
                                        alt="tamneak-icon" width="20">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header Inner -->
        <div class="header-inner">
            <div class="container">
                <div class="cat-nav-head">
                    <div class="row">
                        @if(Request::is('/'))
                        <div class="col-lg-3">
                            <div class="all-category">
                                <div id="mySidepanel" class="sidepanel">
                                    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><i
                                            class="fa fa-close float-right"></i></a>
                                    {!! xss_clean(show_navigation(get_option('category_menu'), 'main-category','sub-category', 'sub-category','right')) !!}
                                </div>
                                <button class="openbtn" onclick="openNav()">
                                    <i class="fa fa-bars" aria-hidden="true"></i> {{ _lang('CATEGORIES') }}</button>
                            </div>

                        </div>
                        @endif

                        <div class="{{ Request::is('/') ? 'col-lg-9 col-12' : 'col-lg-12' }}">
                            <div class="menu-area">
                                <!-- Main Menu -->
                                <nav class="navbar navbar-expand-lg">
                                    <div class="navbar-collapse">
                                        <div class="nav-inner">
                                            {!! xss_clean(show_navigation(get_option('primary_menu'), 'nav main-menu
                                            menu navbar-nav', 'dropdown', 'dropdown sub-dropdown')) !!}
                                        </div>
                                    </div>
                                </nav>
                                <!--/ End Main Menu -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/ End Header Inner -->
    </header>
    <!--/ End Header -->
    <div class="tm-content">
        @yield('content')
    </div>
    <!-- Quick View Shop -->
    <div class="modal fade" id="quickShop" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close"
                            aria-hidden="true"></span></button>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>
    <!-- Quick View Shop end -->
    <!-- Start Footer Area -->
    <footer class="footer">
        <!-- Footer Top -->
        <div class="footer-top section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-12 col-12">
                        <!-- Single Widget -->
                        <div class="single-footer about">
                            <div class="logo">
                                <a href="{{ url('') }}"><img src="{{ get_logo() }}" alt="logo" width="200"></a>
                            </div>
                            <div class="slogan-text">
                                <p>កន្លែងដែលផ្តល់បទពិសោធន៍ថ្មីសម្រាប់យើងទាំងអស់គ្នា</p>
                            </div>

                        </div>
                        <!-- End Single Widget -->
                    </div>
                    <div class="col-lg-6 col-md-12 col-12">
                        <!-- Single Widget -->
                        <div class="single-footer about mt-3">
                            <div class="footer-address-word">
                                <h3>អាសយដ្ឋាន</h3>
                            </div>
                            <p class="text">{!! xss_clean(get_trans_option('footer_about_us')) !!}</p>
                        </div>
                        <!-- End Single Widget -->
                    </div>
                </div>
            </div>
        </div>
        <!-- End Footer Top -->
        <div class="copyright">
            <div class="container">
                <div class="inner">
                    <div class="row">
                        <div class="col-lg-12 col-12">
                            <div class="center">
                                <p class="text-center">{!! xss_clean(get_trans_option('copyright_text')) !!}</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- /End Footer Area -->
    <!-- Load Facebook SDK for JavaScript -->
    <!-- Messenger Chat Plugin Code -->
    <!-- Messenger Chat Plugin Code -->
    <div id="fb-root"></div>

    <!-- Your Chat Plugin code -->
    <div id="fb-customer-chat" class="fb-customerchat">
    </div>
    <!-- Jquery -->
    <script>
        var chatbox = document.getElementById('fb-customer-chat');
        chatbox.setAttribute("page_id", "2006809719546497");
        chatbox.setAttribute("attribution", "biz_inbox");
        window.fbAsyncInit = function() {
            FB.init({
                xfbml            : true,
                version          : 'v10.0'
            });
        };

        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <script src="{{ asset('public/theme/default/js/jquery.min.js') }}"></script>

    <script src="{{ asset('public/theme/default/js/jquery-migrate-3.0.0.js') }}"></script>

    <script src="{{ asset('public/theme/default/js/jquery-ui.min.js') }}"></script>
    <!-- Popper JS -->
    <script src="{{ asset('public/theme/default/js/popper.min.js') }}"></script>
    <!-- Bootstrap JS -->
    <script src="{{ asset('public/theme/default/js/bootstrap.min.js') }}"></script>
    <!-- Slicknav JS -->
    <script src="{{ asset('public/theme/default/js/slicknav.min.js') }}"></script>
    <!-- Owl Carousel JS -->
    <script src="{{ asset('public/theme/default/js/owl-carousel.js') }}"></script>
    <!-- Magnific Popup JS -->
    <script src="{{ asset('public/theme/default/js/magnific-popup.js') }}"></script>
    <!-- Waypoints JS -->
    <script src="{{ asset('public/theme/default/js/waypoints.min.js') }}"></script>
    <!-- Countdown JS -->
    <script src="{{ asset('public/theme/default/js/finalcountdown.min.js') }}"></script>
    <!-- Nice Select JS -->
    <script src="{{ asset('public/theme/default/js/nicesellect.js') }}"></script>
    <!-- Flex Slider JS -->
    <script src="{{ asset('public/theme/default/js/flex-slider.js') }}"></script>
    <!-- ScrollUp JS -->
    <script src="{{ asset('public/theme/default/js/scrollup.js') }}"></script>
    <!-- Onepage Nav JS -->
    <script src="{{ asset('public/theme/default/js/onepage-nav.min.js') }}"></script>
    <!-- Easing JS -->
    <script src="{{ asset('public/theme/default/js/easing.js') }}"></script>

    <script src="{{ asset('public/backend/plugins/jquery-toast-plugin/jquery.toast.min.js') }}"></script>

    <script src="{{ asset('public/theme/default/js/typeahead.bundle.js') }}"></script>

    <script src="{{ asset('public/backend/assets/js/print.js') }}"></script>

    <!-- Active JS -->
    <script src="{{ asset('public/theme/default/js/active.js') }}"></script>


    <!-- Custom JS -->
    @yield('js-script')
</body>

</html>