@extends('theme.default.website')

@section('content')
<!-- Banner Slider Area -->
<div class="slider">
	<div id="carouselExampleIndicators" class="carousel slide" data-interval="8000" data-ride="carousel">
		<ol class="carousel-indicators">
			@foreach($slide as $item)
				<li class="banner-indicator {{$loop->iteration==1 ? 'active' : ''}}" data-target="#carouselExampleIndicators" data-slide-to="{{$loop->iteration}}"></li>
			@endforeach
		</ol>
		<div class="carousel-inner">
			@foreach($slide as $item)
				<div class="carousel-item {{$loop->iteration==1 ? 'active' : ''}}">
					<div class="carousel-image-item">
						<img class="d-block w-100" src="{{ gcpUrl($item->file->file_path) }}" alt="First slide">
					</div>
				</div>
			@endforeach
		</div>
		<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
</div>
<!--/ End Banner Slider Area -->

@if(get_option('enable_three_column_banner') == 1)
	<!-- Start Small Banner  -->
	<section class="small-banner section">
		<div class="container-fluid">
			<div class="row">

				<!-- Single Banner  -->
				<div class="col-lg-4 col-md-6 col-12">
					<div class="single-banner">
						<div class="product-banner">
							<h3>{{ get_trans_option('three_column_1_title') }}</h3>
							<p>{!! xss_clean(get_trans_option('three_column_1_sub_title')) !!}</p>
						</div>
						<a href="{{ get_trans_option('three_column_1_button_link') }}" class="shop-now-btn">
							<p>{{ get_trans_option('three_column_1_button_text') }}</p>
						</a>
						<img src="{{ get_option('three_column_1_background_image') != '' ? gcpUrl(get_option('three_column_1_background_image'),'512x512') : asset('public/theme/default/images/no-banner.jpg') }}" alt="#">
					</div>
				</div>
				<!-- /End Single Banner  -->


				<!-- Single Banner  -->
				<div class="col-lg-4 col-md-6 col-12">
					<div class="single-banner">
						<div class="product-banner">
							<h3>{{ get_trans_option('three_column_2_title') }}</h3>
							<p>{!! xss_clean(get_trans_option('three_column_2_sub_title')) !!}</p>
						</div>
						<a href="{{ get_trans_option('three_column_2_button_link') }}" class="shop-now-btn">
							<p>{{ get_trans_option('three_column_2_button_text') }}</p>
						</a>
						<img src="{{ get_option('three_column_2_background_image') != '' ? gcpUrl(get_option('three_column_2_background_image'),'512x512') : asset('public/theme/default/images/no-banner.jpg') }}" alt="#">

					</div>
				</div>
				<!-- /End Single Banner  -->

				<!-- Single Banner  -->
				<div class="col-lg-4 col-12">
					<div class="single-banner tab-height">
						<div class="product-banner">
							<h3>{{ get_trans_option('three_column_3_title') }}</h3>
							<p>{!! xss_clean(get_trans_option('three_column_3_sub_title')) !!}</p>
						</div>
						<a href="{{ get_trans_option('three_column_3_button_link') }}" class="shop-now-btn">
							<p>{{ get_trans_option('three_column_3_button_text') }}</p>
						</a>
						<img src="{{ get_option('three_column_3_background_image') != '' ? gcpUrl(get_option('three_column_3_background_image'),'512x512') : asset('public/theme/default/images/no-banner.jpg') }}" alt="#">
					</div>
				</div>
				<!-- /End Single Banner  -->

			</div>
		</div>
	</section>
	<!-- End Small Banner -->
@endif


@if(get_option('enable_trending_items') == 1)
<!-- Start Product Area -->
<div class="product-area section">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="section-title">
					<h2>{{ get_trans_option('trending_items_title') }}</h2>
				</div>
			</div>
		</div>

		@php
			$ids = get_option('trending_categories');
			$ids_ordered = implode(',', $ids);
			$trending_categories = \App\Entity\Category\Category::whereIn('id',$ids)->with(["translation","default_lang","products","products.translation","products.default_lang","products.files"])->orderByRaw("FIELD(id, $ids_ordered)")->get();

		 @endphp

		<div class="row">
			<div class="col-12">
				<div class="product-info">
					@foreach($trending_categories as $trending_category)
						@if(count($trending_category->products)>0)
						@include('theme.default.components.category-title',['category_name' => $trending_category->translation->name,'link' => url('/categories/'.$trending_category->slug)])
						<div class="tab-content" id="myTabContent">
							<!-- Start Single Tab -->
								<div class="tab-pane fade show active"  role="tabpanel">
									<div class="tab-single">
										<div class="row">
											@php $products = $trending_category->products->where('is_active',1)->take(8); @endphp
											@include('theme.default.components.product-grid',['class' => 'col-xl-3'])
										</div>
									</div>
								</div>
						<!--/ End Single Tab -->
						</div>
						@endif
					@endforeach
			</div>
		</div>
	</div>
</div>
<!-- End Product Area -->
@endif

@if(get_option('enable_hot_items') == 1)
<!-- Start Most Popular -->
<div class="product-area most-popular section">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="section-title">
					<h2>{{ get_trans_option('hot_items_title') }}</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="owl-carousel popular-slider">

					@php $hot_items = \App\Entity\Product\Product::where('is_active',1)->orderBy('created_at','DESC')->with(["translation","default_lang","files"])->take(12)->get(); @endphp
					
					@foreach($hot_items as $hot_item)
						<!-- Start Single Product -->
						<div class="single-product">
							<div class="product-img">
								<a href="{{ url('product/'.$hot_item->slug) }}">
									<img class="default-img" src="{{ gcpUrl($hot_item->image->file_path,'256x256') }}" alt="{{ $hot_item->translation->name==""?$hot_item->default_lang[0]->name:$hot_item->translation->name }}">
									@if($hot_item->in_stock == 0)
										<span class="out-of-stock">{{ _lang('Out Of Stock') }}</span>
									@elseif($hot_item->featured_tag != NULL)
										<span class="{{ $hot_item->featured_tag }}">{{ _dlang(str_replace('_',' ',$hot_item->featured_tag)) }}</span>
									@endif
								</a>
								<div class="button-head">
									<div class="product-action">
										<a href="{{ url('product/'.$hot_item->slug) }}" title="{{ _lang('Quick View') }}" class="quick-shop">
											<i class=" ti-eye"></i><span>{{ _lang('Quick Shop') }}</span>
										</a>
										<!-- <a title="{{ _lang('Wishlist') }}" class="btn-wishlist" href="{{ wishlist_url($hot_item) }}"><i class=" ti-heart "></i><span>{{ _lang('Add to Wishlist') }}</span></a> -->
									</div>
									<div class="product-action-2">
										<a title="View product detail" class="add_to_cart" data-type="simple_product" href="{{ url('product/'.$hot_item->slug) }}">មើលព័ត៌មានលម្អិត</a>
									</div>
								</div>
							</div>

							<div class="product-content">
								<h3><a href="{{ url('product/'.$hot_item->slug) }}">{{ $hot_item->translation->name==""?$hot_item->default_lang[0]->name:$hot_item->translation->name }}</a></h3>
								
								@if($hot_item->product_type != 'variable_product')
									<div class="product-price">		
										@if($hot_item->special_price != '' || (int) $hot_item->special_price != 0 )
											<span class="text-danger">
												<s>{!! xss_clean(show_price($hot_item->price)) !!}</s>
											</span>
											<span class="text-success">{!! xss_clean(show_price($hot_item->special_price)) !!}</span>
										@else
											<span>{!! xss_clean(show_price($hot_item->price)) !!}</span>	
										@endif
									</div>
								@else
									<div class="product-price">		
										@if($hot_item->variation_prices[0]->special_price != '' || (int) $hot_item->variation_prices[0]->special_price != 0 )
											<span class="text-danger">
												<s>{!! xss_clean(show_price($hot_item->variation_prices[0]->price)) !!}</s>
											</span>
											<span class="text-success">
												{!! xss_clean(show_price($hot_item->variation_prices[0]->special_price)) !!} 
												- 
												{!! xss_clean(show_price($hot_item->variation_prices[count($hot_item->variation_prices) - 1]->special_price)) !!}
											</span>
											
										@else
											<span>
												{!! xss_clean(show_price($hot_item->variation_prices[0]->price)) !!}
												- 
												{!! xss_clean(show_price($hot_item->variation_prices[count($hot_item->variation_prices) - 1]->price)) !!}
											</span>	
										@endif
									</div>
								@endif



							</div>
						</div>
						<!-- End Single Product -->
					@endforeach
					
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Most Popular Area -->

<!-- Start Shop Home List  -->
<section class="shop-home-list section">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-6 col-12">
				<div class="row">
					<div class="col-12">
						<div class="shop-section-title">
							<h1>{{ get_trans_option('on_sale_items_title') }}</h1>
						</div>
					</div>
				</div>

				@php $on_sale_items = \App\Entity\Product\Product::where('is_active',1)->whereIn('id', get_option('on_sale_items',[]))->with(["translation","default_lang","files"])->get(); @endphp

				@foreach($on_sale_items as $on_sale_item)
					<!-- Start Single List  -->
					<div class="single-list">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-12">
								<div class="list-image overlay">
									<img src="{{ gcpUrl($on_sale_item->image->file_path,'256x256') }}" alt="#">
									<a href="{{ url('product/'.$on_sale_item->slug) }}" class="buy"><i class="fa fa-eye"></i></a>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-12 no-padding">
								<div class="content">
									<h4 class="title"><a href="{{ url('product/'.$on_sale_item->slug) }}">{{ $on_sale_item->translation->name==""?$on_sale_item->default_lang[0]->name:$on_sale_item->translation->name }}</a></h4>
										
									<div class="product-price">		
										@if($on_sale_item->special_price != '' || (int) $on_sale_item->special_price != 0 )
											<p class="price with-discount">{!! xss_clean(show_price($on_sale_item->special_price)) !!}</p>
										@else
											<p class="price with-discount">{!! xss_clean(show_price($on_sale_item->price)) !!}</p>	
										@endif
									</div>

								</div>
							</div>
						</div>
					</div>
					<!-- End Single List  -->
				@endforeach
				
			</div>
			<div class="col-lg-4 col-md-6 col-12">
				<div class="row">
					<div class="col-12">
						<div class="shop-section-title">
							<h1>{{ get_trans_option('best_seller_title') }}</h1>
						</div>
					</div>
				</div>

				@php 
					$best_seller_items = \App\Entity\Product\Product::where('is_active',1)->whereIn('id', get_option('best_seller_items',[]))->with(["translation","default_lang","files"])->get();
				@endphp

				@foreach($best_seller_items as $best_seller_item)
					<!-- Start Single List  -->
					<div class="single-list">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-12">
								<div class="list-image overlay">
									<img src="{{ gcpUrl($best_seller_item->image->file_path,'256x256') }}" alt="#">
									<a href="{{ url('product/'.$best_seller_item->slug) }}" class="buy"><i class="fa fa-eye"></i></a>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-12 no-padding">
								<div class="content">
									<h4 class="title"><a href="{{ url('product/'.$best_seller_item->slug) }}">{{ $best_seller_item->translation->name==""?$best_seller_item->default_lang[0]->name:$best_seller_item->translation->name }}</a></h4>
										
									<div class="product-price">		
										@if($best_seller_item->special_price != '' || (int) $best_seller_item->special_price != 0 )
											<p class="price with-discount">{!! xss_clean(show_price($best_seller_item->special_price)) !!}</p>
										@else
											<p class="price with-discount">{!! xss_clean(show_price($best_seller_item->price)) !!}</p>	
										@endif
									</div>

								</div>
							</div>
						</div>
					</div>
					<!-- End Single List  -->
				@endforeach
				
			</div>
			<div class="col-lg-4 col-md-6 col-12">
				<div class="row">
					<div class="col-12">
						<div class="shop-section-title">
							<h1>{{ get_trans_option('top_views_title') }}</h1>
						</div>
					</div>
				</div>

				@php 
					$top_views_items = \App\Entity\Product\Product::where('is_active',1)->whereIn('id', get_option('top_views_items',[]))->with(["translation","default_lang","files"])->get();
				@endphp

				@foreach($top_views_items as $top_views_item)
					<!-- Start Single List  -->
					<div class="single-list">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-12">
								<div class="list-image overlay">
									<img src="{{ gcpUrl($top_views_item->image->file_path,'256x256') }}" alt="#">
									<a href="{{ url('product/'.$top_views_item->slug) }}" class="buy"><i class="fa fa-eye"></i></a>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-12 no-padding">
								<div class="content">
									<h4 class="title"><a href="{{ url('product/'.$top_views_item->slug) }}">{{ $top_views_item->translation->name==""?$top_views_item->default_lang[0]->name:$top_views_item->translation->name }}</a></h4>
										
									<div class="product-price">		
										@if($top_views_item->special_price != '' || (int) $top_views_item->special_price != 0 )
											<p class="price with-discount">{!! xss_clean(show_price($top_views_item->special_price)) !!}</p>
										@else
											<p class="price with-discount">{!! xss_clean(show_price($top_views_item->price)) !!}</p>	
										@endif
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- End Single List  -->
				@endforeach
	
			</div>
		</div>
	</div>
</section>
<!-- End Shop Home List  -->
@endif

{{--@include('theme.default.components.services')--}}

@endsection