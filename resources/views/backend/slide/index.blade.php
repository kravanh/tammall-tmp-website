@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="card no-export">

                <div class="card-header d-flex align-items-center">
                    <a class="btn btn-primary btn-xs ml-auto" href="{{ route('slide.create') }}">{{ _lang('Add Slide') }}</a>
                </div>
                <div class="card-body">
                    <table id="1" class="table table-bordered">
                        <thead>
                        <tr>
                            <th>{{ _lang('Id') }}</th>
                            <th>{{ _lang('Thumbnail') }}</th>
                            <th>{{ _lang('Name') }}</th>
                            <th>{{ _lang('Order') }}</th>
                            <th class="text-center">{{ _lang('Action') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($slide as $item)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td class='logo'>
                                <div class="thumbnail-holder">
                                    <img src="{{ gcpUrl($item->file->file_path,'256x256') }}">
                                </div>
                            </td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->order}}</td>
                            <td class="text-center">
                                <div class="dropdown">
                                    <button class="btn btn-light dropdown-toggle btn-xs" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{ _lang('Action') }}
                                        <i class="fas fa-angle-down"></i>
                                    </button>
                                    <form action="{{ action('SlideController@destroy', $item->id) }}" method="post">
                                        {{ csrf_field() }}
                                        <input name="_method" type="hidden" value="DELETE">

                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a href="{{ action('SlideController@edit', $item->id) }}" class="dropdown-item dropdown-edit dropdown-edit"><i class="mdi mdi-pencil"></i> {{ _lang('Edit') }}</a>
                                            <button class="btn-remove dropdown-item" type="submit"><i class="mdi mdi-delete"></i> {{ _lang('Delete') }}</button>
                                        </div>
                                    </form>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>


                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

