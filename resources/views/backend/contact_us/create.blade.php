@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="card no-export">
                <div class="card-body">
                    <table id="1" class="table table-bordered">
                        <thead>
                        <tr>
                            <th>{{ _lang('Id') }}</th>
                            <th>{{ _lang('Name') }}</th>
                            <th>{{ _lang('Email') }}</th>
                            <th>{{ _lang('Phone') }}</th>
                            <th>{{ _lang('Subject') }}</th>
                            <th>{{ _lang('Message') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($contact_us as $item)
                            <tr>
                                <th>{{ $loop->iteration }}</th>
                                <th>{{ $item->name }}</th>
                                <th>{{ $item->email }}</th>
                                <th>{{ $item->phone }}</th>
                                <th>{{ $item->subject }}</th>
                                <th>{!! $item->message !!}</th>

                            </tr>
                        @endforeach
                        </tbody>


                    </table>
                    {{ $contact_us->links() }}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js-script')
    <script src="{{ asset('public/backend/assets/js/datatables/products.js') }}"></script>
@endsection
