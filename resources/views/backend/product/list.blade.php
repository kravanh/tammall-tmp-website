@extends('layouts.app')

@section('content')

	@php
		$page = isset($_GET['page']) ?  $_GET['page'] : 1;
	@endphp
<div class="row">
	<div class="col-lg-12">
		<div class="card no-export">

		    <div class="card-header d-flex align-items-center">
				<form class="form-inline" method="GET" action="{{route('products.search')}}">
						<div class="form-group m-3">
							<label for="inputSearch" class="col-form-label mr-3">Search</label>
							<input value="{{ isset($_GET['name']) ?  $_GET['name'] : '' }}" type="text" class="form-control search-product" id="inputPassword" name="name">
						</div>

						<div class="form-group m-3">
							<label for="inputCategory" class="col-form-label mr-3">Category</label>
							<select class="form-control choose-cate" name="category">
								<option value="0">Choose Category</option>
								@foreach($category as $item)
									<option {{ isset($_GET['category']) ? ($_GET['category']==$item->id ? 'selected' :'') :''}} value="{{$item->id}}">{{$item->translation->name}}</option>
								@endforeach
							</select>
						</div>

						<div class="form-group m-3">
							<label for="inputBrand" class="col-form-label mr-3">Brand</label>
							<select class="form-control choose-brand" name="brand">
								<option value="0">Choose Brand</option>
								@foreach($brand as $item)
									<option {{ isset($_GET['brand']) ? ($_GET['brand']==$item->id ? 'selected' :'') :''}} value="{{$item->id}}">{{$item->translation->name}}</option>
								@endforeach
							</select>
						</div>
						<button type="submit" class="btn btn-success btn-search-pro">
							Search
						</button>
					</form>
				<a class="btn btn-primary btn-xs ml-auto" href="{{ route('products.create') }}">{{ _lang('Add New') }}</a>
			</div>
			<div class="card-body">
				<table id="1" class="table table-bordered">
					<thead>
					    <tr>
							<th>{{ _lang('Id') }}</th>
							<th>{{ _lang('Thumbnail') }}</th>
							<th>{{ _lang('Name') }}</th>
							<th>{{ _lang('Price') }}</th>
							<th>{{ _lang('Category') }}</th>
							<th>{{ _lang('Brand') }}</th>
							<th>{{ _lang('Status') }}</th>

							<th class="text-center">{{ _lang('Action') }}</th>
					    </tr>
					</thead>
					<tbody>
						@foreach($products as $product)
						<tr>
							<th>{{ $loop->iteration }}</th>
							<th>
								<div class="thumbnail-holder">
									<img src="{{ gcpUrl($product->image->file_path,'256x256') }}">
								</div>
							</th>
							<th>{{ $product->default_lang[0]->name }}</th>
							<th>{{ $product->price."  $" }}</th>
							<th>{{ ($product->categories !=null && count($product->categories)>0) ? $product->categories[0]->translation->name : '' }}</th>
							<th>{{ $product->brand->translation->name }}</th>
							<th>{{ $product->is_active==1 ? "Active" : "UnActive" }}</th>
							<th class="text-center">
								<form action="{{action('ProductController@destroy', $product['id'])}}" class="text-center" method="post">
									<a href="{{action('ProductController@edit', $product['id']).'?page='.$page}}" class="btn btn-warning btn-xs"><i class="far fa-edit"></i></a>&nbsp
									{{ csrf_field() }}
									<input name="_method" type="hidden" value="DELETE">
									<button class="btn btn-danger btn-xs btn-remove" type="submit"><i class="fas fa-eraser"></i></button>
								</form>
							</th>
						</tr>
						@endforeach
					</tbody>


				</table>
				{{ $products->withQueryString()->links() }}
			</div>
		</div>
	</div>
</div>

@endsection

@section('js-script')
<script src="{{ asset('public/backend/assets/js/datatables/products.js') }}"></script>
@endsection
